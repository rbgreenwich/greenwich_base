/**
 * An override of the localgov_eu_cookie_compliance js.
 */

/* eslint-disable */

(function setupCookieSettingsBlock($, Drupal, drupalSettings, cookies) {
  Drupal.behaviors.activateCookieSettingsForm = {
    attach(context) {
      // if Drupal.eu_cookie_compliance set const selectedCookieCategories
      const selectedCookieCategories = Drupal.eu_cookie_compliance.getAcceptedCategories();

      setupRadioCheckboxEventSync();

      Drupal.eu_cookie_compliance.setAcceptedCategories(selectedCookieCategories);
      Drupal.eu_cookie_compliance.loadCategoryScripts(selectedCookieCategories);

      // Drupal.eu_cookie_compliance.setPreferenceCheckboxes(selectedCookieCategories);
      setPreferenceCheckboxes(selectedCookieCategories);
      Drupal.eu_cookie_compliance.attachSavePreferencesEvents();

      once(
        'localgov-eu-cookie-compliance-radios',
        '#eu-cookie-compliance-categories',
        context
      ).forEach(displayCheckboxAsRadios);
    },
  };

  function displayCheckboxAsRadios() {
    setTimeout(() => {
      setupSaveSettingsFeedback();
    }, 300);
  }

  function setupRadioCheckboxEventSync() {
    $('.eu-cookie-compliance-category').each((index, element) => {
      const $checkbox = $(':checkbox', element);
      const $radios = $(':radio', element);
      const $radioOn = $(':radio[value=on]', element);
      const $radioOff = $(':radio[value=off]', element);

      $checkbox.on('change', function () {
        if ($(this).is(':checked')) {
          $radioOn.prop('checked', true);
        } else {
          $radioOff.prop('checked', true);
        }
      });

      $radios.on('change', function () {
        if ($(this).val() === 'on') {
          $checkbox.prop('checked', true);
        } else {
          $checkbox.prop('checked', false);
        }
      });
    });
  }

  // pilfered from eu_cookie_compliance.js to use event listeners
  var cookieValueDisagreed =
    typeof drupalSettings.eu_cookie_compliance.cookie_value_disagreed === 'undefined' ||
    drupalSettings.eu_cookie_compliance.cookie_value_disagreed === ''
      ? '0'
      : drupalSettings.eu_cookie_compliance.cookie_value_disagreed;
  function setPreferenceCheckboxes(categories) {
    if (
      Drupal.eu_cookie_compliance.getCookieStatus() !== null ||
      Drupal.eu_cookie_compliance.getCookieStatus() === cookieValueDisagreed
    ) {
      // Unset all categories to prevent a problem where the checkboxes with a
      // default state set would always show up as checked.
      $('#eu-cookie-compliance-categories input:checkbox')
        .not(':disabled')
        .prop('checked', false)
        .trigger('change');
    }
    // Check the appropriate checkboxes.
    for (var i in categories) {
      var categoryElement = document.getElementById('cookie-category-' + categories[i]);
      if (categoryElement !== null) {
        categoryElement.checked = true;
        categoryElement.dispatchEvent(new Event('change'));
      }
    }
  }

  function setupSaveSettingsFeedback() {
    const saveButton = document.querySelector('.eu-cookie-compliance-save-preferences-button');
    const saveButtonLabel = saveButton.innerHTML;
    saveButton.addEventListener(
      'click',
      function () {
        saveButton.innerHTML = `${Drupal.t('Saving')}`;
        saveButton.classList.add('spinning');
        setTimeout(function () {
          saveButton.classList.remove('spinning');
          saveButton.innerHTML = `${Drupal.t('Preferences saved')}`;
        }, 2000);
      },
      false
    );
    saveButton.addEventListener(
      'blur',
      function () {
        saveButton.innerHTML = saveButtonLabel;
      },
      false
    );
  }
})(jQuery, Drupal, drupalSettings, window.Cookies);
