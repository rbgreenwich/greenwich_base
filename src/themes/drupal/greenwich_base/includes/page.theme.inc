<?php

/**
 * @file
 * Functions to support page blocks in greenwich_base theme.
 */

use Drupal\node\NodeInterface;

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function greenwich_base_theme_suggestions_page_alter(array &$suggestions, array $variables) {
  $node = \Drupal::routeMatch()->getParameter('node');
  if ($node instanceof NodeInterface) {
    $suggestions[] = 'page__' . $node->bundle();
  }
}
