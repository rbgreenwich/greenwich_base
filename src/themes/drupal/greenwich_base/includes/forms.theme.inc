<?php

/**
 * @file
 * Theme functions for forms.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * @file
 * Functions to support theming forms in greenwich_base theme.
 */

/**
 * Implements hook_theme_suggestions_HOOK_alter() for form container templates.
 */
function greenwich_base_theme_suggestions_container_alter(array &$suggestions, array $variables) {
  $element = $variables['element'];

  // We cannot count on template_preprocess_container having run, so we copy
  // its logic here to provide templates for forms (has parents) or not forms.
  // Special handling for form elements.
  if (isset($element['#array_parents'])) {
    $suggestions[] = 'container__has_parent';
  }
  else {
    $suggestions[] = 'container__no_parent';
  }

  if (isset($element['#type']) && $element['#type'] != 'container') {
    $suggestions[] = 'container__' . $element['#type'];
  }

  if (isset($element['#type']) && $element['#type'] == 'container' && isset($element['children']['#type'])) {
    $suggestions[] = 'container__' . $element['children']['#type'];
  }

}

/**
 * Implements hook_form_BASE_FORM_ID_alter() for the views_exposed_form form.
 *
 * Moves the 'date range' field to the bottom.
 */
function greenwich_base_form_views_exposed_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if (isset($form['#id']) && $form['#id'] == 'views-exposed-form-localgov-events-listing-page-all-events') {
    // Hide the date range field. @see LMW-129 for reasons.
    $form['date_picker']['dates']['#access'] = FALSE;
    $form['date_picker']['dates']['#title'] = t('Time period');
    if (isset($form['date_picker']['dates']['#options']['choose'])) {
      $form['date_picker']['dates']['#options']['choose'] = t('- Any -');
    }
    $form['neighbourhood']['#weight'] = 1;
    $form['actions']['#weight'] = 2;
    $form['actions']['submit']['#value'] = t('Apply filters');
    $form['clear'] = [
      '#type' => 'inline_template',
      '#template' => "{{ pattern('menu_horizontal', { menu_name: 'Clear filters and search menu', items: [
        {title: 'Clear filters and search', url: url},
      ] }, 'body')}}",
      '#context' => [
        'url' => Url::fromRoute('view.localgov_events_listing.page_all_events')->toString(),
      ],
      '#weight' => 5,
    ];
  }
}
