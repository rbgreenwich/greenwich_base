<?php

/**
 * @file
 * Functions to support theming content layout in greenwich_base theme.
 */

use Drupal\node\NodeInterface;

/**
 * Implements hook_preprocess_HOOK().
 */
function greenwich_base_preprocess_node(&$variables) {

  $node = $variables['node'];
  if ($node instanceof NodeInterface) {
    $is_front = \Drupal::service('path.matcher')->isFrontPage();
    $variables['is_front'] = $is_front;
    if ($is_front) {
      $variables['#cache']['contexts'][] = 'url.path.is_front';
    }
    $node_type = $node->getType();

    // Localgov Outpost Service node.
    if ($node_type == 'localgov_outpost_service' && $variables['view_mode'] = 'full') {
      $variables['outpost_service_id'] = '';
      // Get the original outpost service ID from the migration map table.
      $node_id = $node->id();
      $query = \Drupal::database()->select('migrate_map_outpost_migration_deriver__localgov_outpost_service', 'mm');
      $query->fields('mm', ['sourceid1']);
      $query->condition('mm.destid1', $node_id);
      $source_id = $query->execute()->fetchField();
      if ($source_id) {
        $variables['outpost_service_id'] = $source_id;
      }
      // Min age and max age logic.
      greewich_base_set_suitable_ages($variables);

      // Auto load site-settings variables on full node view mode.
      greenwich_base_load_site_settings($variables);

    }
  }

}

/**
 * Helper function to load the site_settings variables.
 *
 * When the option to 'disable_auto_loading' is set, we need to load our
 * site_settings variables ourselced. This can help with performance.
 *
 * This code is copied form the site_settings.module file.
 * See https://git.drupalcode.org/project/site_settings/-/blob/2.0.0-alpha2/site_settings.module?ref_type=tags#L65-83
 */
function greenwich_base_load_site_settings(&$variables) {
  $config = \Drupal::config('site_settings.config');
  /** @var \Drupal\site_settings\SiteSettingsLoaderPluginManager $plugin_manager */
  $plugin_manager = \Drupal::service('plugin.manager.site_settings_loader');
  $loader = $plugin_manager->getActiveLoaderPlugin();
  if ($loader->allowAutoload()) {
    // Load the site settings into the specified key.
    $template_key = $config->get('template_key');
    $variables[$template_key] = $loader->loadAll();
  }
}

/**
 * Implements hook_preprocess_field().
 */
function greenwich_base_preprocess_field(&$variables) {

  if ($variables['field_name'] == 'localgov_outpost_suitabilities') {
    // Load the current node to access the min-age and max-age values.
    $variables['node'] = \Drupal::request()->attributes->get('node');
    // On migrations, we don't have a node at this point, so we check.
    if ($variables['node']) {
      // Min age and max age logic.
      greewich_base_set_suitable_ages($variables);
    }
  }

}

/**
 * Generate the text for min and max ages.
 *
 * @param array $variables
 *   The variables array passed in from hook_preprocess functions.
 */
function greewich_base_set_suitable_ages(&$variables) {
  $node = $variables['node'];
  // Min age and max age logic.
  $min_age = NULL;
  $max_age = NULL;
  $variables['ages'] = '';
  $min_age = $node->localgov_outpost_age_min ? $node->localgov_outpost_age_min->value : NULL;
  $max_age = $node->localgov_outpost_age_max ? $node->localgov_outpost_age_max->value : NULL;
  // No ages set.
  if (is_null($min_age) && is_null($max_age)) {
    $variables['ages'] = "All ages";
  }
  // Only min age is set.
  if (is_numeric($min_age) && is_null($max_age)) {
    $variables['ages'] = "Ages $min_age and up";
  }
  // Only max age is set.
  if (is_null($min_age) && is_numeric($max_age)) {
    $variables['ages'] = "Ages up to $max_age";
  }
  // Both min age and max age are set.
  if (is_numeric($min_age) && is_numeric($max_age)) {
    $variables['ages'] = "Ages $min_age to $max_age";
  }
}

/**
 * Implements hook_theme_suggestions_node_alter().
 */
function greenwich_base_theme_suggestions_node_alter(array &$suggestions, array $variables) {
  if (\Drupal::service('path.matcher')->isFrontPage()) {
    $suggestions[] = 'node__front';
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter() for field templates.
 */
function greenwich_base_theme_suggestions_field_alter(array &$suggestions, array $variables) {

  // Suggest field--localgov-outpost-service--link.
  $suggestions[] = 'field__' . $variables['element']['#bundle'] . '__' . $variables['element']['#field_type'];

  // Suggest field--localgov-outpost-service--entity-reference--localgov-outpost-suitabilities.
  $suggestions[] = 'field__' . $variables['element']['#bundle'] . '__' . $variables['element']['#field_type'] . '__' . $variables['element']['#field_name'];

  $element = $variables['element'];
  $suggestions[] = 'field__' . $element['#entity_type'] . '__' .
  $element['#field_name'] . '__' . $element['#bundle'] . '__' .
  $element['#view_mode'];
}
