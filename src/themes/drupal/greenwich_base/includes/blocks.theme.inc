<?php

/**
 * @file
 * Functions to support theming blocks in greenwich_base theme.
 */

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function greenwich_base_theme_suggestions_block_alter(array &$suggestions, array $variables) {
  // Provide suggestion for block templates by custom block type.
  if (!empty($variables['elements']['content']['#block_content'])) {
    $block = $variables['elements']['content']['#block_content'];
    // Add `block--BLOCK-TYPE.html.twig`.
    $suggestions[] = 'block__' . $block->bundle();
    $view_mode = $variables['elements']['#configuration']['view_mode'];
    if (!empty($view_mode)) {
      // Add `block--BLOCK-TYPE--VIEW-MODE.html.twig`.
      $suggestions[] = 'block__' . $block->bundle() . '__' . $view_mode;
    }
  }
}

/**
 * Implements hook_preprocess_block().
 */
function greenwich_base_preprocess_block(&$variables) {
  // Tweak behaviour of facet_blocks.
  if ($variables['base_plugin_id'] == 'facet_block') {
    if (!(isset($variables['content'][0]) && isset($variables['content'][0]['#items']) && count($variables['content'][0]['#items']))) {
      // Unset the block content, as we have no facet items.
      unset($variables['content']);
    }
  }
  if ($variables['base_plugin_id'] == 'greenwich_site_footer') {
    greenwich_base_load_site_settings($variables);
  }

}
