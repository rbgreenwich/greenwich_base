<?php

/**
 * @file
 * Functions to support theming page layout in greenwich_base theme.
 */

/**
 * Implements hook_preprocess_HOOK().
 */
function greenwich_base_preprocess_page(&$variables) {
  $moduleHandler = \Drupal::service('module_handler');
  if ($moduleHandler->moduleExists('components')) {
    $variables['has_components_module'] = TRUE;
  }

  // Check we are on a node to count the results, we want a different layout
  // if there are no results.
  if (isset($variables['node'])) {
    $node = $variables['node'];
    $nid = $node->id();
    $bundle = $node->bundle();
    if ($bundle == 'localgov_directory') {
      // Load the view.
      // @todo this could be a significant performance hit, consider other ways.
      $view = \Drupal::entityTypeManager()
        ->getStorage('view')
        ->load('localgov_directory_channel')
        ->getExecutable();
      // Set display and arguments.
      $view->initDisplay();
      $view->setDisplay('node_embed');
      $view->setArguments([$nid]);
      // Execute the view to get the total_rows.
      $view->execute();
      $total_rows = $view->total_rows;
      $variables['total_rows'] = $total_rows;
    }
  }

}
