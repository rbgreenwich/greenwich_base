<?php

/**
 * @file
 * Functions to support theming content layout in greenwich_base theme.
 */

/**
 * Implements hook_theme_suggestions_HOOK_alter() for region templates.
 */
function greenwich_base_theme_suggestions_region_alter(array &$suggestions, array $variables) {
  // Get the current node (if available).
  $node = \Drupal::routeMatch()->getParameter('node');

  if ($node && $node->getType()) {

    // Add a template suggestion based on the content type.
    $suggestions[] = 'region__' . $node->getType();
    $suggestions[] = 'region__' . $node->getType() . '__' . $variables['elements']['#region'];
  }
}

/**
 * Implements hook_preprocess_region().
 */
function greenwich_base_preprocess_region(&$variables) {
  if ($variables['elements']['#region'] === 'sidebar') {
    // Check we are on a node to count the results, we want a different layout
    // if there are no results.
    $current_node = \Drupal::routeMatch()->getParameter('node');
    if ($current_node) {
      $nid = $current_node->id();
      $bundle = $current_node->bundle();
      if ($bundle == 'localgov_directory') {
        // Load the view.
        // @todo this could be a significant performance hit, consider other ways.
        $view = \Drupal::entityTypeManager()
          ->getStorage('view')
          ->load('localgov_directory_channel')
          ->getExecutable();
        // Set display and arguments.
        $view->initDisplay();
        $view->setDisplay('node_embed');
        $view->setArguments([$nid]);
        // Execute the view to get the total_rows.
        $view->execute();
        $total_rows = $view->total_rows;
        $variables['total_rows'] = $total_rows;
      }
    }
  }

}
