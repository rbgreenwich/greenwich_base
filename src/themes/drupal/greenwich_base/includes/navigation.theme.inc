<?php

/**
 * @file
 * Functions to support theming navigation in greenwich_base theme.
 */

use Drupal\system\Entity\Menu;

/**
 * Implements hook_preprocess_HOOK().
 */
function greenwich_base_preprocess_menu(&$variables) {
  $menu = Menu::load($variables['menu_name']);
  if ($menu instanceof Menu) {
    $variables['menu_label'] = $menu->label();
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function greenwich_base_theme_suggestions_pager_alter(array &$suggestions, array $variables) {
  foreach ($variables['pager']['#theme'] as $theme) {
    $suggestions[] = $theme;
  }
}
