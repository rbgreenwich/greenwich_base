<?php

/**
 * @file
 * Functions to preprocess views template variables.
 */

/**
 * Implements hook_preprocess_views_view().
 */
function greenwich_base_preprocess_views_view(&$variables) {
  $view = $variables['view'];
  $view_id = $view->storage->id();
  if ($view_id == "localgov_directory_channel") {
    // Make the search query string available to the template.
    // @todo can we load the actual string name from the view such that changes
    // to the query string name in the view don't break this?
    $search_string = \Drupal::request()->query->get('search');
    $variables['search_string'] = $search_string;
  }
}
