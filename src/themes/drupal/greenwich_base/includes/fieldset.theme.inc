<?php

/**
 * @file
 * Fieldset theme functions.
 */

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function greenwich_base_theme_suggestions_fieldset_alter(array &$suggestions, array $variables, $hook) {
  if (isset($variables['element']['#array_parents'][0])) {
    $id = str_replace("-", "_", $variables['element']['#array_parents'][0]);
    $suggestions[] = $hook . '__' . $id;
  }
  elseif (isset($variables['element']['#id'])) {
    $id = str_replace("-", "_", $variables['element']['#id']);
    $suggestions[] = $hook . '__' . $id;
  }
}
