/**
 * Global namespaces
 */

const path = require('path');

const wsdesignsystem = path.resolve(__dirname);
const wspatterns = path.resolve(wsdesignsystem, 'patterns');

module.exports = {
  wsdesignsystem,
  wspatterns,
  tokens: path.resolve(wsdesignsystem, 'tokens'),
  clientside: path.resolve(wsdesignsystem, 'clientside'),
  protons: path.resolve(wspatterns, '00-protons'),
  atoms: path.resolve(wspatterns, '01-atoms'),
  molecules: path.resolve(wspatterns, '02-molecules'),
  organisms: path.resolve(wspatterns, '03-organisms'),
  templates: path.resolve(wspatterns, '04-templates'),
  pages: path.resolve(wspatterns, '05-pages'),
};
