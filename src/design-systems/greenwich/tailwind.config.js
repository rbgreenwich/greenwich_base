/* eslint-disable global-require */

const presets = require('../../../tailwind.config');

module.exports = {
  prefix: 'tw-',
  content: [
    './src/apps/storybook/drupal_component_library/**/*.{yml,twig,scss,js}',
    './src/design-systems/greenwich/**/*.{yml,twig,scss,js}',
    './src/themes/greenwich_base/**/*.{yml,twig,scss,js}',
  ],
  corePlugins: {
    preflight: false,
  },
  plugins: [
    // https://tailwindcss.com/docs/plugins.
  ],
  presets: [
    // https://tailwindcss.com/docs/presets.
    require('../../../tailwind/config/colors'),
    require('../../../tailwind/config/typography'),
    presets, // Must be last.
  ],
};
/* eslint-enable global-require */
