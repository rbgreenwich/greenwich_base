import 'regenerator-runtime/runtime';
import Alpine from 'alpinejs';
import collapse from '@alpinejs/collapse';
import { exampleAlpineJsBehavior } from 'examples/example-alpine-js/example-alpine-js.behavior';
import { headerBehavior } from '../../patterns/03-organisms/header/header.behavior';
import { headerGcdBehavior } from '../../patterns/03-organisms/header-gcd/header-gcd.behavior';
import { filterAccordionBehavior } from '../../patterns/03-organisms/filter-accordion/filter-accordion.behavior';

Alpine.plugin(collapse);

Drupal.behaviors.alpinejs = {
  isStarted: false,

  attach() {
    this.registerEvents();
    this.start();
  },

  /**
   * Register custom Alpine data functions.
   */
  alpineData() {
    exampleAlpineJsBehavior();
    headerBehavior();
    headerGcdBehavior();
    filterAccordionBehavior();
  },

  start() {
    this.alpineData();

    if (this.isStarted === false) {
      Alpine.start();
      window.Alpine = Alpine;
    }
  },

  registerEvents() {
    document.addEventListener('alpine:initialized', () => {
      if (this.isStarted === false) {
        this.isStarted = true;
      }
    });
  },
};
