import 'regenerator-runtime/runtime';
import breakpointHelper from 'breakpoint-helper';

// see src/design-systems/greenwich/styles/tools/breakpoint-helper for where values are added to the page

const bph = breakpointHelper('meta');

const breakpointDefaults = {
  isLtPhone: bph.isMatching('phone', true), // max width 480px
  isLtTablet: bph.isMatching('tablet', true), // max width 768px
  isLtDesktop: bph.isMatching('desktop', true), // max width 1024px
  isLtDesktopWide: bph.isMatching('desktop-wide', true), // max width 1440px
  isGtPhone: bph.isMatching('phone'), // min width 480px
  isGtTablet: bph.isMatching('tablet'), // min width 768px
  isGtDesktop: bph.isMatching('desktop'), // min width 1024px
  isGtDesktopWide: bph.isMatching('desktop-wide'), // min width 1440px
  isPhoneOnly: bph.isMatching('phone', true), // max width 480px
  isTabletOny: bph.isMatching(['phone', 'tablet'], true), // min width 480px max width 768px
  isDesktopOnly: bph.isMatching(['tablet', 'desktop'], true), // min width 768px max-width 1024
  isDesktopWideOnly: bph.isMatching('desktop'), // min width 1024
};

// 'phone': 480px,
// 'tablet': 768px,
// 'desktop': 1024px,
// 'desktop-wide': 1440px,

window.Breakpoint = breakpointDefaults;

export const breakpoint = breakpointDefaults;

const breakpointStatus = (e, name) => {
  const { matches } = e;
  const capitalizedName = `${name.charAt(0).toUpperCase()}${name.slice(1)}`;

  window.Breakpoint[`is${capitalizedName}`] = matches;
  breakpoint[`is${capitalizedName}`] = matches;

  return matches;
};

const isLtPhone = bph.listen(
  {
    name: 'phone',
    useMax: true,
    immediate: true,
  },
  (e) => breakpointStatus(e, 'LtPhone')
);

const isLtTablet = bph.listen(
  {
    name: 'tablet',
    useMax: true,
    immediate: true,
  },
  (e) => breakpointStatus(e, 'LtTablet')
);

const isLtDesktop = bph.listen(
  {
    name: 'desktop',
    useMax: true,
    immediate: true,
  },
  (e) => breakpointStatus(e, 'LtDesktop')
);

const isLtDesktopWide = bph.listen(
  {
    name: 'desktop-wide',
    useMax: true,
    immediate: true,
  },
  (e) => breakpointStatus(e, 'LtDesktopWide')
);

const isGtPhone = bph.listen(
  {
    name: 'phone',
    useMax: false,
    immediate: true,
  },
  (e) => breakpointStatus(e, 'GtPhone')
);

const isGtTablet = bph.listen(
  {
    name: 'tablet',
    useMax: false,
    immediate: true,
  },
  (e) => breakpointStatus(e, 'GtTablet')
);

const isGtDesktop = bph.listen(
  {
    name: 'desktop',
    useMax: false,
    immediate: true,
  },
  (e) => breakpointStatus(e, 'GtDesktop')
);

const isGtDesktopWide = bph.listen(
  {
    name: 'desktop-wide',
    useMax: false,
    immediate: true,
  },
  (e) => breakpointStatus(e, 'GtDesktopWide')
);

const isPhoneOnly = bph.listen(
  {
    name: 'phone',
    useMax: true,
    immediate: true,
  },
  (e) => breakpointStatus(e, 'PhoneOnly')
);

const isTabletOnly = bph.listen(
  {
    name: ['phone', 'tablet'],
    useMax: true,
    immediate: true,
  },
  (e) => breakpointStatus(e, 'TabletOnly')
);

const isDesktopOnly = bph.listen(
  {
    name: ['tablet', 'desktop'],
    useMax: true,
    immediate: true,
  },
  (e) => breakpointStatus(e, 'DesktopOnly')
);

const isDesktopWideOnly = bph.listen(
  {
    name: 'desktop',
    useMax: false,
    immediate: true,
  },
  (e) => breakpointStatus(e, 'DesktopWideOnly')
);

export const BreakpointHelper = {
  ...bph,
  isLtPhone,
  isLtTablet,
  isLtDesktop,
  isLtDesktopWide,
  isGtPhone,
  isGtTablet,
  isGtDesktop,
  isGtDesktopWide,
  isPhoneOnly,
  isTabletOnly,
  isDesktopOnly,
  isDesktopWideOnly,
};

window.BreakpointHelper = BreakpointHelper;
