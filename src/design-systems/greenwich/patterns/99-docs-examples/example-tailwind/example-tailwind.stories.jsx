import './index';

const patternDefinition = require('./example-tailwind.wingsuit.yml');
patternDefinition.example_tailwind.namespace = 'Documentation/Component examples';
export const wingsuit = {
  patternDefinition,
  parameters: {
    layout: 'fullscreen',
  },
};
