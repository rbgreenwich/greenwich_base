/**
 * example-component
 */
import './example-component.behavior';
import './example-component.twig';
import './example-component.wingsuit.yml';
