import './index';

const patternDefinition = require('./example-component.wingsuit.yml');
patternDefinition.example_component.namespace = 'Documentation/Component examples';
export const wingsuit = {
  patternDefinition,
  parameters: {
    layout: 'padded',
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/nUvPqUZ9QBE72qaB7H0y3c/RBG-Design-System?type=design&node-id=647-5580&mode=design&t=0glDr2UKCNl7kDN3-0',
    },
  },
};
