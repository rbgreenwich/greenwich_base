import './index';

const patternDefinition = require('./example-alpine-js.wingsuit.yml');

patternDefinition.example_alpine_js.namespace = 'Documentation/Component examples';
export const wingsuit = {
  patternDefinition,
  parameters: { layout: 'padded' },
};
