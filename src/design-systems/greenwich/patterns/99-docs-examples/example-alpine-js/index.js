/**
 * example-alpine-js
 */
import './example-alpine-js.twig';
import './example-alpine-js.wingsuit.yml';
import './example-alpine-js.behavior';

export const name = 'exampleAlpineJs';
