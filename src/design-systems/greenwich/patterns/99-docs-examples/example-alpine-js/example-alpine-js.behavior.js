import 'regenerator-runtime/runtime';
import Alpine from 'alpinejs';

export const exampleAlpineJsBehavior = () => {
  Alpine.data('exampleAlpineJsBehavior', () => ({
    open: false,
    $exampleAlpineJs: false,
    toggle() {
      this.open = !this.open;
    },
    init() {
      console.log('init');
      this.$exampleAlpineJs = this.$el;
    },
  }));
};
