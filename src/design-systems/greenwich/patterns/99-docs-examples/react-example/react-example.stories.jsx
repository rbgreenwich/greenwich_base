import './index';
import ReactExample from './ReactExample';
import patternDefinition from './react-example.wingsuit.yml';

patternDefinition.react_example.namespace = 'Documentation/Component examples';
export const wingsuit = {
  component: ReactExample,
  patternDefinition,
  parameters: {
    layout: 'fullscreen',
  },
};
