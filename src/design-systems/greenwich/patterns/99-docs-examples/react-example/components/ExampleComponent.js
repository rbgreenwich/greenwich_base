import React from 'react';
import PropTypes from 'prop-types';

function ExampleComponentWithProps(props) {
  const { title, copy } = props;

  return (
    <div>
      <h1 className="tw-text-center tw-text-3xl tw-mb-5">{title}</h1>
      <p className="tw-text-center tw-text-base tw-mb-5">{copy}</p>
    </div>
  );
}
ExampleComponentWithProps.propTypes = {
  title: PropTypes.string.isRequired,
  copy: PropTypes.string.isRequired,
};

export default ExampleComponentWithProps;
