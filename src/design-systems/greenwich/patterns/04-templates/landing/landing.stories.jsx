import './index';

const patternDefinition = require('./landing.wingsuit.yml');

export const wingsuit = {
  patternDefinition,
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/nUvPqUZ9QBE72qaB7H0y3c/RBG-Design-System?type=design&node-id=1914-2643&mode=design&t=ZhjK7EZJDAFIQgGm-0',
    },
  },
};
