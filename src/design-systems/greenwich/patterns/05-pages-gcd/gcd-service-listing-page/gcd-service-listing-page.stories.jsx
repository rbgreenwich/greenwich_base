import './index';

const patternDefinition = require('./gcd-service-listing-page.wingsuit.yml');

export const wingsuit = {
  patternDefinition,
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/nUvPqUZ9QBE72qaB7H0y3c/RBG-Design-System?type=design&node-id=1842-2159&mode=design&t=QPqXNWUSJfZu7XQL-0',
    },
  },
};
