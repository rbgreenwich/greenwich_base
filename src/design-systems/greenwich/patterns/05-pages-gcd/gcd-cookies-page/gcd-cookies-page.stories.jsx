import './index';

const patternDefinition = require('./gcd-cookies-page.wingsuit.yml');

export const wingsuit = {
  patternDefinition,
};
