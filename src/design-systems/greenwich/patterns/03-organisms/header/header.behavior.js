import 'regenerator-runtime/runtime';
import Alpine from 'alpinejs';

// @TODO need to update this behaviour to not close submenus
export const headerBehavior = () => {
  Alpine.data('headerBehavior', () => ({
    activeAccordion: '',
    drawerOpen: false,
    asideText: undefined,
    setActiveAccordion(id) {
      this.activeAccordion = this.activeAccordion === id ? '' : id;
      console.log(`activeAccordion: ${this.activeAccordion}`);
      this.drawerOpen = !this.drawerOpen;
    },
    init() {
      //
    },
    toggleAsideText() {
      if (this.asideText === undefined) {
        // Save the original text the first time this function is called
        this.asideText = this.$refs.asideMenu.innerText;
      }

      const spanElement = this.$refs.asideMenu.querySelector('span');
      if (this.$refs.asideMenu.innerText === 'Close') {
        // If the text is 'Close', set it back to the original text
        spanElement.textContent = this.asideText;
      } else {
        // Otherwise, set it to 'Close'
        spanElement.textContent = 'Close';
      }
    },
    bindSetActiveAccordion: {
      // eslint-disable-next-line func-names
      '@click': function () {
        this.setActiveAccordion(this.target);
        if (this.asideMenu) this.toggleAsideText();
      },
      // eslint-disable-next-line func-names
      '@keydown.escape.window': function () {
        this.drawerOpen = false;
        this.activeAccordion = '';
      },
    },
  }));
};
