import './index';
import docs from './docs.mdx';

const patternDefinition = require('./header.wingsuit.yml');

export const wingsuit = {
  patternDefinition,
  parameters: {
    docs: {
      page: docs,
    },
  },
};
