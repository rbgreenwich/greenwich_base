/**
 * link
 */
import './header.behavior';
import './header.twig';
import './header.wingsuit.yml';

export const name = 'header';
