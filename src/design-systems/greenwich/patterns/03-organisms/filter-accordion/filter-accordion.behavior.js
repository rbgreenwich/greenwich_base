import 'regenerator-runtime/runtime';
import Alpine from 'alpinejs';
import { breakpoint } from 'rbgscripts/vendorjs/breakpoint-helper.vendor';

export const filterAccordionBehavior = () => {
  Alpine.data('filterAccordionBehavior', () => ({
    $accordion: false,
    filtersOpen: !breakpoint.isLtDesktop,
    windowWidth: window.innerWidth,
    init() {
      this.$accordion = this.$el;
    },
    bindFilterAccordion: {
      // eslint-disable-next-line func-names
      '@resize.window.stop.debounce': function () {
        // Check window width has actually changed and it's not just iOS triggering a resize event on scroll
        if (window.innerWidth !== this.windowWidth) {
          this.filtersOpen = !breakpoint.isLtDesktop;
        }
      },

      // eslint-disable-next-line func-names
      ':class': function () {
        return {
          open: this.filtersOpen,
          closed: !this.filtersOpen,
        };
      },
    },
    bindFilterAccordionButton: {
      // eslint-disable-next-line func-names
      '@click': function () {
        this.filtersOpen = !(this.filtersOpen && breakpoint.isLtDesktop);
      },
    },
  }));
};
