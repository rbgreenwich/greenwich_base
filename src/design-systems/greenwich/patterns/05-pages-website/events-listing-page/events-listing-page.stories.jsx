import './index';

const patternDefinition = require('./events-listing-page.wingsuit.yml');

export const wingsuit = {
  patternDefinition,
};
