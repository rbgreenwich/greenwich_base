import 'regenerator-runtime/runtime';
import Alpine from 'alpinejs';

// @TODO need to update this behaviour to not close submenus
export const headerBehavior = () => {
  Alpine.data('FilterAccordionItemBehavior', () => ({
    activeAccordion: '',
    drawerOpen: false,
    asideText: undefined,
    setActiveAccordion(id) {
      this.activeAccordion = this.activeAccordion === id ? '' : id;
      console.log(`activeAccordion: ${this.activeAccordion}`);
      this.drawerOpen = !this.drawerOpen;
    },
    init() {
      //
    },
    bindSetActiveAccordion: {
      // eslint-disable-next-line func-names
      '@click': function () {
        this.setActiveAccordion(this.target);
      },
      // eslint-disable-next-line func-names
      '@keydown.escape.window': function () {
        this.activeAccordion = '';
      },
    },
  }));
};
