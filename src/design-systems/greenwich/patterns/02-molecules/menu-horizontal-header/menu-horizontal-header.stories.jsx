import './index';

const patternDefinition = require('./menu-horizontal-header.wingsuit.yml');

export const wingsuit = {
  patternDefinition,
};
