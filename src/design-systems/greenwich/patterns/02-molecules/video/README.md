drupal adds a whole bunch of extra divs in the real code so it ends up looking more like this:

```twig
<div {{ attributes.addClass(classes | sort | join(' ') | trim) }}>
  {# This represents the divs that the media template adds (with the contextual stuff) #}
  <div class="contextual-region">
    {# title_suffix.contextual_links #}
    <div data-contextual-id="media:media=5:changed=1697622017&amp;langcode=en" data-contextual-token="_lERr_fLG8SgZRC1oHtQP60msmMe27zF1OJ5-BvtOqU" data-drupal-ajax-container=""></div>
      {# this uses media-oembed-iframe.html.twig #}
      {# @TODO media-embed-error.html.twig #}
      {# {{content}} #}
      <iframe src="http://gcd-drupal.ddev.site:8080/media/oembed?url=https%3A//youtu.be/_C9fTDBUHPE&amp;max_width=960&amp;max_height=960&amp;hash=npmLC9BFxgpxJh9XpjKn2vbQLa13uJxLN4Mb3oBt1ZQ" frameborder="0" allowtransparency width="960" height="540" class="media-oembed-content" loading="eager" title="Dementia talk from a carer"></iframe>
    </div>
  </div>
</div>
```
