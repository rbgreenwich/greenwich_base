import './index';

const patternDefinition = require('./menu-horizontal.wingsuit.yml');

export const wingsuit = {
  patternDefinition,
};
