import './index';

const patternDefinition = require('./divider.wingsuit.yml');

export const wingsuit = {
  patternDefinition,
};
