import './index';

const patternDefinition = require('./hint.wingsuit.yml');

export const wingsuit = {
  patternDefinition,
};
