import './index';

const patternDefinition = require('./logo.wingsuit.yml');

export const wingsuit = {
  parameters: {
    rootClasses: ['tw-w-1/2'],
  },
  patternDefinition,
};
