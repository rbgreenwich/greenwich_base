/**
 * scripts
 * In order to include everything in storybook we need this file
 * Drupal picks up everything through a different set of webpack settings
 * See also repos/design_system/src/design-systems/greenwich/namespaces.js
 */
import 'rbgstyles';
import 'rbgscripts';
