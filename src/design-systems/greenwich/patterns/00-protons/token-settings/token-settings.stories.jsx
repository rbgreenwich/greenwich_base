import './index';
import mdx from './token-settings.mdx';

const patternDefinition = require('./token-settings.wingsuit.yml');

export const wingsuit = {
  patternDefinition,
  parameters: {
    docs: {
      page: mdx,
    },
  },
};
