import './index';

const patternDefinition = require('./form-element-label.wingsuit.yml');

export const wingsuit = {
  patternDefinition,
};
