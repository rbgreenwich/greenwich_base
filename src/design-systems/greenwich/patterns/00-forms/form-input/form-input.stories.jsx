import './index';

const patternDefinition = require('./form-input.wingsuit.yml');

export const wingsuit = {
  patternDefinition,
};
