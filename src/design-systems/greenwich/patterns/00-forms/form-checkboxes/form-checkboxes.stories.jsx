import './index';

const patternDefinition = require('./form-checkboxes.wingsuit.yml');

export const wingsuit = {
  patternDefinition,
};
