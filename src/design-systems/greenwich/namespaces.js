/**
 * Global namespaces
 */

const path = require('path');

const wsdesignsystem = path.resolve(__dirname);
const wspatterns = path.resolve(wsdesignsystem, 'patterns');

module.exports = {
  wsdesignsystem,
  wspatterns,
  rbgstyles: path.resolve(wsdesignsystem, 'styles'),
  rbgscripts: path.resolve(wsdesignsystem, 'scripts'),
  clientside: path.resolve(wsdesignsystem, 'clientside'),
  macros: path.resolve(wspatterns, '_macros'),
  examples: path.resolve(wspatterns, '99-docs-examples'),
  protons: path.resolve(wspatterns, '00-protons'),
  forms: path.resolve(wspatterns, '00-forms'),
  atoms: path.resolve(wspatterns, '01-atoms'),
  molecules: path.resolve(wspatterns, '02-molecules'),
  organisms: path.resolve(wspatterns, '03-organisms'),
  templates: path.resolve(wspatterns, '04-templates'),
  pages_website: path.resolve(wspatterns, '05-pages-website'),
  pages_gcd: path.resolve(wspatterns, '05-pages-gcd'),
};
