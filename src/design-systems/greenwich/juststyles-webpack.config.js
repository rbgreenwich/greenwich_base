/**
 * Wingsuit webpack config for theme files only
 */

const svg = require('@wingsuit-designsystem/core/dist/server/presets/svg');
const wingsuitCore = require('@wingsuit-designsystem/core');
const scss = require('../../../presets/scss');
const themeAssets = require('../../../presets/theme-assets');

const appConfig = wingsuitCore.resolveConfig('drupal_greenwich_base', process.env.NODE_ENV);

// this has to be done here, as wingsuitCore.resolveConfig merges the default presets back in
appConfig.presets = [
  themeAssets,
  svg,
  scss,
  // '@wingsuit-designsystem/preset-scss',
  '@wingsuit-designsystem/preset-tailwind2',
  '@wingsuit-designsystem/preset-postcss8',
];
const finalConfig = wingsuitCore.getAppPack(appConfig);

module.exports = finalConfig;
