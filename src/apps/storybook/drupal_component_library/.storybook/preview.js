import { configure, initJsBehaviors } from '@wingsuit-designsystem/storybook';
import { TwingRenderer } from '@wingsuit-designsystem/pattern';
import { addDecorator, addParameters } from '@storybook/react';
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';
import {
  gcdThemeingConfig,
  gcdThemeingDecorator,
} from './../../greenwich_design_system/theme-switcher';
const renderImpl = new TwingRenderer();

const namespaces = require('../../../../design-systems/greenwich/namespaces');
const { tailwind } = require('../config/silo/tailwind.json');

const { colors } = tailwind.theme;

initJsBehaviors('Drupal');

addParameters({
  options: {
    storySort: {
      method: 'alphabetical',
      order: [
        'Welcome',
        'Documentation',
        ['Greenwich design system', 'Getting started'],
        'Protons',
        'Forms',
        ['Forms'],
        'Atoms',
        'Molecules',
        'Organisms',
        'Templates',
        'Pages',
        'Pages-website',
        ['Website pages'],
        'Pages-gcd',
        ['Greenwich community directory pages'],
        'Tokens',
        'Tools',
      ],
      locales: 'en-UK',
    },
  },

  viewport: {
    viewports: {
      ...INITIAL_VIEWPORTS,
    },
  },

  backgrounds: {
    grid: {
      disable: true,
    },
    values: Object.keys(colors).map((key) => {
      const color = typeof colors[key] === 'string' ? colors[key] : colors[key].DEFAULT || false;
      const name = `${key.charAt(0).toUpperCase()}${key.slice(1)}`;

      return {
        name,
        value: color,
      };
    }),
  },

  globalTypes: {
    ...gcdThemeingConfig.globalTypes,
  },

  layout: 'fullscreen',
});

addDecorator(gcdThemeingDecorator);

configure(
  module,
  [
    require.context('./../patterns', true, /\.stories(\.jsx|\.js|\.mdx)$/),
    require.context('wspatterns', true, /\.stories(\.jsx|\.js|\.mdx)$/),
  ],
  require.context('./config', false, /\.json|\.ya?ml$/),
  require.context('wspatterns', true, /\.twig$/),
  namespaces,
  renderImpl
);
