const wingsuitCore = require('@wingsuit-designsystem/core');
const postCss = require('postcss');

module.exports = {
  addons: [
    '@storybook/addon-docs',
    '@storybook/addon-controls',
    '@storybook/addon-essentials',
    '@storybook/addon-viewport',
    '@storybook/addon-links',
    '@storybook/addon-measure',
    '@storybook/addon-outline',
    'addon-screen-reader',
    '@storybook/addon-a11y',
    'storybook-addon-designs',
    {
      name: '@storybook/addon-postcss',
      options: {
        postcssLoaderOptions: {
          implementation: postCss,
        },
      },
    },
  ],

  webpackFinal: (config, { configType }) => {
    const final = wingsuitCore.getAppPack(wingsuitCore.resolveConfig('drupal_component_library'), [
      config,
    ]);

    return final;
  },
};
