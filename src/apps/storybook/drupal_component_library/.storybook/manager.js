// .storybook/manager.js

import { addons } from '@storybook/addons';
// import theme from '@wingsuit-designsystem/storybook/dist/theme';
import theme from './theme';

addons.setConfig({
  theme,
  showToolbar: true,
  showNav: true,
  showPanel: true,
  enableShortcuts: true,
  panelPosition: 'right',
});
