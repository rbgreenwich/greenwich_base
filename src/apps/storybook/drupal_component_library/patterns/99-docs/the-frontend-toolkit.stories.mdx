import { Meta, Title, Source } from '@storybook/addon-docs';
import spacingFileRaw from '!raw-loader!rbgstyles/tokens/spacing/_index.scss';
import spacingMixinFileRaw from '!raw-loader!rbgstyles/tokens/spacing/mixins/_spacing.scss';

export const spacingRaw = spacingFileRaw;
export const spacingMixinRaw = spacingMixinFileRaw;

<Meta
  title="Documentation/The Frontend Toolkit"
  parameters={{ viewMode: 'docs', previewTabs: { canvas: { hidden: true } } }}
/>

<Title>The Frontend Toolkit</Title>

The frontend toolkit is a series of scss modules and mixins that allow us to develop quickly.

It also allows us to use design tokens and swap them out easily in order to apply themes to components.

# Conventions

- ⚠️ The component library is _component only_. This means that each module has its own css there are **no global styles**.
- We're using the `BEM` naming convention. `header`, `header__title`, `header__title--active`.
- We're using `ITCSS` to name some things.
- When naming components avoid using theme names and be as descriptive as possible. `navigation` and `navigation--multi-level` is preferable to `navigation` and `navigation--intranet`. **The design system documentation should indicate where and how a component is used**
- ⚠️ In the design system, **colour** is always spelt `color`.
- When importing scss modules name them consistently.

## Design tokens

- design token values can be found [here](https://www.figma.com/file/nUvPqUZ9QBE72qaB7H0y3c/RBG-Design-System?type=design&node-id=732-4689&t=091jUDOtQMCWts9t-0).
- Currently the design system can be found in the following [figma file](https://www.figma.com/file/nUvPqUZ9QBE72qaB7H0y3c/RBG-Design-System?type=design&node-id=626-3253&t=091jUDOtQMCWts9t-0).

# Understanding the frontend tokens and how they work

The intention of this document is to show how the code is setup to call the various frontend tokens, and explain what some of the code inside those tokens is doing. Where possible we link out to the documentation pages inside this design system along with the scss documentation.

Our goal here is to demonstrate how to update the spacing token to include additional outputs when it is called.

## Frontend tokens

The convention for frontend tokens is to be called using the syntax `@include spacing.apply` or `@include spacing.apply-space`.

In nearly all cases the plural mixin will generate multiple results and the singular will apply only one instance, usually plural mixin is simply calling the singular multiple times

Take for example the spacing mixin **`@include spacing.apply`**. [More documentation for this mixin can be found here](/?path=/docs/documentation-frontend-toolkit-token-reference-spacing--margin#mixin-apply)

This mixin outputs multiple classes. This is used when a component has a setting with multiple options available, we simply call the mixin inside the components scss and then apply the correct class when the setting is set.
Colors are another good example, rather than write out the code multiple times to add all our colors, we can keep that logic in one place.

Here is some psudo code to demonstrate whats happening. Though in some places in the actual code some parts are abstracted out in order to make the code more DRY.

```
// theme.scss
$theme: (
  colors: (
    'red': '#123455',
    'yellow': '#ff81d6'
  )
);

// tokens/color.scss

// outputs
// .color--red { color: #123455 }
// .color--yellow { color: #ff81d6 }

@mixin colors-apply($theme) {
  $colors: $theme.colors;
  for each $color in $colors {
    .color--#{$color} {
      color: $color
    }
  }
}


// component.wingsuit.yml
// NB background color setting is actually inside another componetn and brought into multiple places to be more DRY

settings:
  background_color:
    label: Background color
    options:
      red: Red
      yellow: Yellow


// component.twig


{% set selector = 'rbg-component-name' %}
{% set color = red %}

{% set classes = [
  selector,
  color ? 'color--' ~ color
] %}

<div class="{{classes}}"></div>

// the output is:


<div class="rbg-component-name color--red"></div>



// _component.scss
// glossing over some of the component details here as that is covered further in the component-only documentation
.rbg-component-name {

  @include colors.init($theme);
  @include colors.apply;

}


// outputs

.rbg-component-name .color--red { color: red; }
.rbg-component-name .color--yellow { color: yellow; }

```

## Demo: How to update the spacing token to include additional outputs when it is called.

The task here is to update the spacing tokens to add in additional classes for responsive spacing.

In order to make it easier to see what code is being generated

- Create a new file in `src/design-systems/greenwich/styles` called `test.scss` (note there is no `_`, this is because this file is not a [partial](https://sass-lang.com/documentation/at-rules/use/#partials))
- You can build this file by running `make build-theme-greenwich-juststyles theme=website` (the theme part just makes it a bit quicker)
- You can open the file by running `code dist/themes/drupal/greenwich_base/test.css`

The starting point for all debugging is the `main--*.scss` file, we'll replicate the basics that we need inside our `test.scss` file

### Bringing in the theme

```scss
@use 'themes/royal-borough-greenwich-website' as theme-config;
```

The first line we will need to add to `test.scss` is the theme we'll be working with, this line calls the module 'royal-borough-greenwich-website'.

We're using [SCSS modules](https://sass-lang.com/documentation/at-rules/use/#loading-members) in our codebase to keep our code DRY, enable reuse of code for theming.

Inside the royal-borough-greenwich-website module is some logic that ultimately generates one big scss map of values for each theme, [you can read more about maps here](https://sass-lang.com/documentation/values/maps/). This theme map is what contains the values for the design tokens we have in our designs. Maps are similar to javascript objects.

NB: Sass has [built in methods for dealing with maps](https://sass-lang.com/documentation/modules/map/) which we do use occasionally, but they are limited so we have [added a helper function](http://localhost:3010/?path=/docs/documentation-frontend-toolkit-tool-reference-utilities--page#get) `utils.get()` which lets us access values at different depths in the map.

When we @use our them, we're also giving it a name, this isn't required but is best practise in our codebase so that code can be reused easily.

NB: SASS has [meta methods](https://sass-lang.com/documentation/modules/meta/). These are useful for debugging how our modules work, you can use the `module-variables`, `module-functions` and `module-mixins` methods to see what is being returned from a particular module.

Unfortunately theres no nicely formatted way of viewing the data but you can copy it out of the command line when you build the scss file, and put `$output: ` in front of it, and your editor will be able to auto format it.

```scss
@use 'sass:meta';
@use 'themes/greenwich-community-directory' as theme-config;

@debug meta.module-variables('theme-config');
@debug '-------------------';
@debug meta.module-functions('theme-config');
@debug '-------------------';
@debug meta.module-mixins('theme-config');
```

When you output the variables from `@debug meta.module-variables('theme-config');` you can see we get a map, the map contains two keys at the top level.

`@use 'themes/greenwich-community-directory' as theme-config;` is outputting the content of the file `src/design-systems/greenwich/styles/themes/royal-borough-greenwich-website/_index.scss` this file uses the [@forward](https://sass-lang.com/documentation/at-rules/forward/) method to forward additional config data. Inside of the `src/design-systems/greenwich/styles/themes/royal-borough-greenwich-website/config/_index.scss` file there are two variables, `$theme-config` and `$theme`, these are the two variables shown inside our `@debug meta.module-variables('theme-config');` output.

NB: As a convention we're using \_index.scss files and using folder names to name modules, this makes it closer to how javascript behaves. We use the `@forward` and `@use` methods inside these files to allow us to keep our \_scss files shorter. In scss modules **each** file is treated as a module, so we use @forward and @use as a way to get around that and keep things simpler.

### Adding in the spacing token

```scss
@use 'themes/greenwich-community-directory' as theme-config;
@use 'rbgstyles/tokens/spacing' as spacing;
```

After we've added in our theme we need to include token module, in this case this is a straightforward @use, this is becasue we use the init function to set default values later on.

NB: Components use `@use` slightly differently we made the decision that components **must** have a **$theme** variable else they will fail. Since we don't currently have any testing in place its best to ensure that when a component is loaded its loaded with the correct theme.

Tokens have a bit more leeway, and have been setup so that each one if it is not initiated with `init()` will fetch the values it needs eg spacing tokens from the default theme. This does make the code self documenting in terms of its dependencies but does run the risk of loading a gcd frontend token with website tokens, though the code does output a warning.

It's also done this way because when a component is initialised we're using a mixin to output the styles in order to avoid duplicating our code as well as making the output much cleaner. This means however that we don't have access to the current theme object inside the `_component.scss` so we're getting around this by including the `init()` method each time we use a token.

The as name with () syntax [is documented here](https://sass-lang.com/documentation/at-rules/use/#configuration)

```scss
@use 'forms/form-element/styles' as form-element with (
  $theme: theme-config.$theme
);
```

### Initialising the spacing token

```scss
@use 'themes/greenwich-community-directory' as theme-config;
@use 'rbgstyles/tokens/spacing' as spacing;

@include spacing.init(theme-config.$theme);
@include spacing.apply(false);
```

As mentioned above we follow the convention of `name.init($theme)` and `name.apply($args)`.

`@include spacing.init(theme-config.$theme);` is given the standardised $theme object from the theme-config (greenwich-community-directory) module.

`@include spacing.apply(false);` The arguments for each frontend token will vary, and we will cover these further down.

For now we have replicated the code for including a mixin in a page, if you wanted to 'mock' up a component and see its output you can simply wrap your code in a selector.

```scss
@use 'themes/greenwich-community-directory' as theme-config;
@use 'rbgstyles/tokens/spacing' as spacing;

.mock-component {
  @include spacing.init(theme-config.$theme);
  @include spacing.apply(false);
}
```

### Inside the spacing component

The `@use 'rbgstyles/tokens/spacing' as spacing;` line is loading the `src/design-systems/greenwich/styles/tokens/spacing/_index.scss` file, which I will document here:

<details>
  <summary>The `_index.scss` file</summary>
  <Source code={spacingRaw} language="scss" />
</details>

Each frontend token `_index.scss` file follows the same convention and file structure.

- spacing/ (frontend-token-name)
  - mixins/
    - Contains the code for each mixin the module exports, similar to javascript modules
  - utils/
    - contains utility [@functions](https://sass-lang.com/documentation/at-rules/function/), either things used within the spacing module itself or things that are output as mixins, calc-spacing() is used inside the /mixins but is also very useful inside components because of our [responsive typography](/?path=/docs/documentation-frontend-toolkit-typography--page)
  - \_index.scss (brings together everything)
  - \_spacing.scss (contains a map with the design token values from the designs)

Inside of the file we will import any helpers that are needed for the `_index.scss` file to function.

Then we will import any other tokens that are needed inside the mixins, for example if the spacing mixin needed breakpoints and colors tokens, and the flow token needed just breakpoints, we would import both breakpoints and colors tokens here, and setup a `$theme` object that mimics the full $theme object it should be passed. This is just as a precaution

### Inside a mixin

<details>
  <summary>The `mixins/_spacing.scss` file</summary>
  <Source code={spacingMixinRaw} language="scss" />
</details>

# Theming

Theming works by either creating a new theme for the design system or you can init the component with a new config object, this is helpful in some cases.

```scss
@use 'examples/example-component/styles' as example-component with (
  $theme: theme-config.$theme
);

// -----------------------------
// EXAMPLE COMPONENTS
// -----------------------------
// NB doesn't need init() for most of these as the module defaults are website themed by default
// However this component has a different variant for gcd so we're overwriting things here.
$example-component-config: (
  'reversed': true,
  'background': (
    'background-color': utils.get('colors.gcd-orange.300', theme-config.$theme),
  ),
  'cta': (
    'border-color': utils.get('colors.gcd-orange.300', theme-config.$theme),
  ),
) !default;

@include example-component.init($example-component-config);
@include example-component.apply;
```

# Troubleshooting

Use `@debug` and the scss meta module to help debug issues. Replicating the problem in the scss playground is also helpful.

**My component theme styles aren't loading?**  
Ensure you have the `@content;` line in your `_component.scss`
