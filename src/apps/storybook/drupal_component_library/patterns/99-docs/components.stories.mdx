import { Meta, Story, Canvas, Title, Subtitle, Source } from '@storybook/addon-docs';
import dedent from 'dedent';
import './images/01-create-a-component.png';
import './images/02-create-a-component.png';
import './images/03-create-a-component.png';
import './images/04-create-a-component.png';
import './images/component-design.png';

<Meta
  title="Documentation/Components"
  parameters={{ viewMode: 'docs', previewTabs: { canvas: { hidden: true } } }}
/>

<Title>Components</Title>
<Subtitle>Everything you need to know</Subtitle>

There are several steps to creating a component, the design, the styling and the configuration.

⚠️ The design system is designed to be _component only_. This means that each module has its own css there are **no global styles**. There are a few reasons for this which are explained below.

Avoid duplicating components where possible, instead add settings or variants to existing components.

**Naming components**

When naming components avoid using theme names and be as descriptive as possible. `navigation` and `navigation--multi-level` is preferable to `navigation` and `navigation--intranet`. **The design system documentation should indicate where and how a component is used**

- [Designing a component](#designing-a-component)
  - [Theming](#theming)
- [Component only design](#component-only-design)
  - [Component only design in scss](#component-only-design-in-scss)
- [Structure of a component](#structure-of-a-component)
- [Creating a new component](#creating-a-new-component)
  - [Generate the component](#generate-the-component)
  - [Setup the component](#setup-the-component)

# Designing a component

Components are designed in figma by the designers. We're using design tokens to identify and display values for each component using the components configuration.

Components can be nested, instead of defining the body styles or header styles on each component, these can be defined once and brought in to more advanced components.

Example of a component made up of other components

<div style={{ margin: 'auto', position: 'relative', marginBottom: '40px' }}>
  <img src="images/component-design.png" style={{ width: '100%' }} />
</div>

## Theming

- ⚠️ The `website` theme is considered the default theme.
- The website, Greenwich community directory, intranet all have different themes defined.
- As much as possible this component library supports theming. This means both changing the values of defaults as well as extending components for theme variants.
- If the website does not use the component, **it is considered good practice to follow the module conventions and put all theme related css and javascript within the theme specific files**. The documentation will indicate whether or not that module is used in each site.
- If a themed component differs vastly from the website component and you find yourself undoing parent CSS, its ok to create a new component! Just don't name it using the theme name.
- We're not using body classes for the different themes, each site has its own CSS with the css for that theme so we don't end up with multiple themes in one css file

# Component only design

Since we will be using these components in different libraries, Local gov drupal, Sharepoint etc we need to ensure we have as consistent (as possible!) set of defaults to work with on our components. However we don't want to risk contaminating existing components on those pages.

For this reason, rather than use `reset.css` and set any top level elements we include a mixin from the reset module, which gives us a standard working environment within each component.

We're also then able to use other modules like the `headings` module, to bring in our standard headings if we know that headings will even be used in the component we're creating.

For example.

```scss
@mixin headings {
  h1,
  h2,
  h3 {
    font-family: 'Arial', sans-serif;
  }
  h1 {
    font-size: utils-get('headings.h1.size', $cc);
  }
}

// doesn't actually use h1 in the standard sense
.header {
  &__title {
    font-size: utils-get('header.title.size', $cc);
  }
}

// We're going to use headers here!
.content-block {
  @include headings;
}
```

Outputs the following:

```css
.header__title {
  font-size: 22px;
}

.content-block h1,
.content-block h2,
.content-block h3 {
  font-family: 'Arial', sans-serif;
}
.content-block h1 {
  font-size: 30px;
}
```

We also use this approach for any other standard elements that will be present, things like default text styling,link styles, hover styles, focus styles anything that you would normally set on the root css stylesheet.

While this approach does give us more css for each component it ensures as much as possible that our components remain consistent and can be tested and viewed in isolation without changes to the visuals.

- NB there is currently one instance of @at-root adding to root in the reset module, the duplicate line will be removed by post-css.

## Component only design in scss

Normally to create a component in scss you would do something like this:

```scss
// style.scss
.rbg-header {
  &__logo {
    background-image: url('website.png');
  }
}
```

which gives you this css

```css
/* style.css */
.rbg-header__logo {
  background-image: url('website.png');
}
```

When you add in theming we typically do this by using a body class to indicate which theme we're using and we use this value in the component to make the changes to the component accordingly.

```scss
// style.scss
.rbg-header {
  &__logo {
    background-image: url('website.png');
  }

  // change logo for gcd theme
  .theme-gcd & {
    .rbg-header__logo {
      background-image: url('gcd.png');
    }
  }
}
```

this gives you this css

```css
/* style.css */
.rbg-header__logo {
  background-image: url('website.png');
}
.theme-gcd .rbg-header .rbg-header__logo {
  background-image: url('gcd.png');
}
```

The problem with this approach is:

- Each site you apply this style.css file to gets all of the theme styles
- This can unexpected visual issues on sites
- Leads to spaghetti CSS thats difficult to debug
- code becomes difficult to onboard developers to without a lot of historical knowledge or knowledge of all of the designs
- In our case each theme doesn't relate at all to the other, themes usually mean switching colours but in our case we don't want to include the gcd styles in the website code, or gcd and website styles in the intranet styles

When you add more themes in you get something like this, which when your building for multiple platforms can increase complexities between the different themes even more

```scss
// styles.scss
.rbg-header {
  &__logo {
    background-image: url('website.png');
  }

  // change logo for gcd theme
  .theme-gcd & {
    .rbg-header__logo {
      background-image: url('gcd.png');
    }
  }

  // change logo for intranet theme
  .theme-intranet & {
    .rbg-header__logo {
      background-image: url('gcd.png');
    }
  }

  // website and intrant have red border gcd has green
  .theme-gcd &,
  .theme-intranet & {
    border: 1px solid red;
  }

  // gcd is used on drupal and we need to include this
  // code to account for that
  .theme-gcd .drupal-specific-class & {
    border-color: green;
    .rbg-header__logo {
    }
  }
}
```

outputs this css

```css
/* style.css */
.rbg-header__logo {
  background-image: url('website.png');
}
.theme-gcd .rbg-header .rbg-header__logo {
  background-image: url('gcd.png');
}
.theme-intranet .rbg-header .rbg-header__logo {
  background-image: url('gcd.png');
}
.theme-gcd .rbg-header,
.theme-intranet .rbg-header {
  border: 1px solid red;
}
.theme-gcd .drupal-specific-class .rbg-header {
  border-color: green;
}
```

To solve these problems we treat and create each theme individually by branching off a common base point this means that

- developers only need to know about the one theme, and not worry about affecting others
- debugging becomes easier and simpler
- we can put in platform specific workarounds without affecting other versions
- its quicker to onboard and develop
- less unpredictable regression between deploys and commits, my work on the gcd won't affect any work on the intranet and vice versa.
- much cleaner code output

There is an assumption with this approach that the component we're creating is generally the same on each site, and that the amount of customisation per site is reasonably achievable through css and templating.

A lot of this is mitigated by breaking down components into atoms, molecules and organisms but if the component is so vastly different then we can just create a new component.

We do this approach by still taking advantage of css's cascading features to overwrite parts of the previous css. We can also use scss features like @extend to copy css from previous selectors to reuse them to save us re-writing things.

We can also create mixins specific to the component that can be used in each theme to help write more reusable styles.

In the simplest terms this is what we're writing:

```scss
// website.css
.rbg-header {
  &__logo {
    background-image: url('website.png');
  }
}

// gcd.css
.rbg-header {
  &__logo {
    background-image: url('website.png');
  }

  // gcd theme
  &__logo {
    background-image: url('gcd.png');
  }
}

// other.css
.rbg-header {
  &__logo {
    background-image: url('website.png');
  }

  // other theme
  &__logo {
    background-image: url('other.png');
  }
}
```

```css
/* website.css */
.rbg-header__logo {
  background-image: url('website.png');
}

/* gcd.css */
.rbg-header__logo {
  background-image: url('website.png');
}
.rbg-header__logo {
  background-image: url('gcd.png');
}

/* other.css */
.rbg-header__logo {
  background-image: url('website.png');
}
.rbg-header__logo {
  background-image: url('other.png');
}
```

Using more scss features such as mixins and the `@content` feature it looks like this:

```scss
@mixin component() {
  .rbg-header {
    &__logo {
      background-image: url('website.png');
    }

    @content;
  }
}

// website.css
@include component;

// gcd.css
@include component {
  &__logo {
    background-image: url('gcd.png');
  }
}
```

```css
/* website.css */
.rbg-header__logo {
  background-image: url('website.png');
}

/* gcd.css */
.rbg-header__logo {
  background-image: url('website.png');
}
.rbg-header__logo {
  background-image: url('gcd.png');
}
```

and to demonstrate it with some of the logic for theme selection we get something that looks like this:

```scss
// component/_component.css
@mixin component() {
  .rbg-header {
    &__logo {
      background-image: url('website.png');
    }
    @content;
  }
}

// component/themes/_gcd.scss
@mixin component-gcd {
  @include component {
    &__logo {
      background-image: url('gcd.png');
    }
  }
}

// component/themes/_index.scss
@mixin load-component-theme($theme: null, $args...) {
  @if $theme == 'gcd' {
    @include component-gcd($args...);
  }
}

// component/_index.scss
@mixin apply($theme) {
  @if $theme == default {
    // basic styles from component mixin
    @include component;
  } @else {
    // basic styles from component mixin but
    // with our theme specifics added to the end
    @include load-component-theme($theme);
  }
}

// styles/website.css
$theme: default;
@include apply($theme);

// styles/gcd.css
$theme: gcd;
@include apply($theme);
```

```css
/* website.css */
.rbg-header__logo {
  background-image: url('website.png');
}

/* gcd.css */
.rbg-header__logo {
  background-image: url('website.png');
}
.rbg-header__logo {
  background-image: url('gcd.png');
}
```

We're adding this process on top of some other patterns, such as:

**Component based design**

We're keeping our component css entirely separate and bringing in code that would typically be inherited from the top of the stylesheet in order to generate entirely standalone components

- the components aren't affected by other styles in the page
- few dependencies, the components don't affect other styles on the page
- we can update and amend token styles in one place, abstracting out tedious things like generating responsive typography on each page and overwriting them to one place, it makes for cleaner and easier to maintain components
- it acts as a code smell as well if the components begin to look overly complicated then it indicates that the component should be broken down further, either within the design system, or within the component itself into a series of modules
- we're using scss modules to do a lot of this abstraction, its very powerful, especially as our CSS isn't tied closely to our markup, as it would be if we were using something like react components.

```scss
// tokens/typography.scss
@mixin typography {
  p {
    font-size: 1rem;
  }
  p.lede {
    font-size: 1.2rem;
  }
}
// tokens/headings.scss
@mixin headings {
  h1 {
    font-size: 2rem;
  }
  h2 {
    font-size: 1.8rem;
  }
  h3 {
    font-size: 1.8rem;
  }
}

// components/header.scss
.rbg-header {
  @include headings;
  @include typography;
  &__logo {
    background-image: url('website.png');
  }
}

// components/content.scss
.rbg-rich-text {
  @include headings;
  @include typography;
  p.lede {
    // this can be further enhanced using theme config
    font-size: 1.4rem;
  }
}
```

**Theme and component configuration**

Using scss modules allows us to think of css as if it were javascript. SCSS Modules lets us bring in or export only the code we need to build our components. We can also pass through maps of values to be used to store values specific to the theme, and we can further enhance functionality to allow for more customisable components as well using component configuration maps.

Certain functionality isn't there yet in scss so the setup is a little verbose but theres debug tools available to work out what is being sent to where.

All of these processes and techniques combine to give us the workflow we have at the moment. Alongside other things such as wingsuit, which informs a lot of the file and folder structure and markup requirements as well.

# Structure of a component

Each component in this component library contains:

- **`component.twig` file**

This file contains the html markup for a component, it can be rendered using PHP or Javascript.

- **`component.wingsuit.yml` file**

This file does several things:

1. it acts as documentation for the component
1. the documentation is used to generate controls inside of storybook so you can quickly see what a component looks like with a setting toggled on or off
1. it lets you describe variants of a component so you can quickly view it in different states, especially useful when integrating with drupal or doing visual regression testing

- **`component.stories.jsx` file**

This is used by storybook to know how best to display the component

- **`styles/*.scss` folder**

This includes all of the style information for the component, you can read more about this in the frontend toolkit section.

# Creating a new component

## Generate the component

- Run `make generate-component` in the design system folder
- Choose `drupal_component_library` from the options

  <div style={{ margin: '2rem', padding: '1rem', border: '1px solid #f1f1f1', width: '60%' }}>
    <img src="images/01-create-a-component.png" />
  </div>

- Select the folder you want the component to be created in

  <div style={{ margin: '2rem', padding: '1rem', border: '1px solid #f1f1f1', width: '60%' }}>
    <img src="images/02-create-a-component.png" />
  </div>

- Name your component, you don't have to name it programatically, so `List item` is fine

  <div style={{ margin: '2rem', padding: '1rem', border: '1px solid #f1f1f1', width: '60%' }}>
    <img src="images/03-create-a-component.png" />
  </div>

- Select `Wingsuit component (UI Pattern)`, you can learn more about the other options in the [wingsuit documentation](https://wingsuit-designsystem.github.io/components/overview/)

  <div style={{ margin: '2rem', padding: '1rem', border: '1px solid #f1f1f1', width: '60%' }}>
    <img src="images/04-create-a-component.png" />
  </div>

- Do you need a CSS file? - **select no**  
   Do you need a SCSS file? - **select no**  
   Do you need an javascript attach event handler? - **select no**  
   How do you want to document your component? - **select automatic**

## Setup the component

- Copy the styles folder from `~generator` into your new component
- Edit the `component/styles/_config.scss` file and enter the selector for your component, `rbg-component` is the convention
- Update your template to include the selector and add it to the classes array

export const code = `{% set selector = 'rbg-component' %}
    {% set classes = [
      selector
    ] %}
    `;

<Source language="twig" dark format={false} code={dedent`${code}`} />

- Add references to your new component in the main stylesheets `repos/design_system/src/design-systems/greenwich/styles/main-website.scss` and `repos/design_system/src/design-systems/greenwich/styles/main-gcd.scss`
- Your component is setup and ready to be developed - read up on the [[frontend toolkit]] or [[developing with drupal]] next
