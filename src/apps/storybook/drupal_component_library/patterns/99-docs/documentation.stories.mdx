import {
  Meta,
  Story,
  Canvas,
  Title,
  Subtitle,
  Description,
  Source,
  ArgsTable,
  ColorPalette,
  ColorItem,
  IconGallery,
  IconItem,
} from '@storybook/addon-docs';
import {
  PatternProperties,
  PatternDoc,
  PatternLoad,
  PatternPreview,
  PatternInclude,
} from '@wingsuit-designsystem/storybook';
import dedent from 'ts-dedent';

<Meta
  title="Documentation/Writing documentation"
  parameters={{ viewMode: 'docs', previewTabs: { canvas: { hidden: true } } }}
/>

<Title>Writing documentation</Title>
<Subtitle>Documentation on how to write documentation!</Subtitle>

There are many different ways and approaches to write documentation in the design system, this section assumes you have the design system [setup and running](/docs/documentation-getting-started--page).

## Storybook documentation

<Subtitle>Documentation that is only displayed on the storybook sites</Subtitle>

As mentioned in the [introduction to the Greenwich design system](/docs/documentation-greenwich-design-system--page#apps) there are three different storybooks in the repository and each of those contains a patterns folder, which is where documentation lives.
This page can be found at `src/apps/storybook/drupal_component_library/patterns/99-docs/documentation.stories.mdx`, and is the simplest form of documentation

### Iframes

You can use iframes inside mdx to show documentation from other sources

<iframe src="https://rbgreenwich.gitlab.io/dev/design_system/" width="100%" height="400" />

### Figma documentation

Inside the stories.js you can include a link to the figma file, this will add a new tab to show the figma file in the design system

```js
export const wingsuit = {
  patternDefinition,
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/nUvPqUZ9QBE72qaB7H0y3c/RBG-Design-System?type=design&node-id=1914-2643&mode=design&t=ZhjK7EZJDAFIQgGm-0',
    },
  },
};
```

### .mdx documentation

This type of documentation can be used to create single pages, like this one, or it can be used in place of the automatically generated markdown for components. It can also be combined with other files to create more interative demonstrations, similar to those seen in the frontend toolkit section.

Documentation is written in `.mdx` format and the filename **must** end in `.stories.mdx` for it to be picked up. Documentation written in `.mdx` files can bring in any react components and use the components defined in the [`@storybook/addon-docs` addon](https://storybook.js.org/docs/get-started).

You can read more about the [storybook docs addon here](https://release-6-5--storybook-frontpage.netlify.app/docs/6.5/writing-docs/introduction) and the additional blocks provided by [wingsuit here](https://wingsuit-designsystem.github.io/documentation/mdx/)

At a minimum your documentation page should contain the following

```mdx
import { Meta } from '@storybook/addon-docs';

<Meta
  title="Documentation/Page title"
  parameters={{ viewMode: 'docs', previewTabs: { canvas: { hidden: true } } }}
/>
```

The Meta block tells storybook where to place the page in the navigation, which can be further refined using the `preview.js` file for the storybook your working in.

See below for more examples of documentation blocks you can use.

## FAQ & Troubleshooting

**It's broken/not displaying**  
Check the terminal or the developer console for an error. Sometimes you can force a rebuild by just adding a space to a file, other times you just have to restart the application or refresh your browser.

If your using an autoformatter sometimes it can mistake mdx for md, try removing content and adding it back in slowly until you find where the error is.

If in doubt, write your markdown in a separate application.

**How do I change the order pages are shown in?**  
You can change this by editing the `preview.js` file in the .storybook folder for the storybook your working with

## Documentation examples

### Colours!

<ColorPalette>
  <ColorItem
    title="theme.color.greyscale"
    subtitle="Some of the greys"
    colors={{ White: '#FFFFFF', Alabaster: '#F8F8F8', Concrete: '#F3F3F3' }}
  />
  <ColorItem title="theme.color.primary" subtitle="Coral" colors={{ WildWatermelon: '#FF4785' }} />
  <ColorItem title="theme.color.secondary" subtitle="Ocean" colors={{ DodgerBlue: '#1EA7FD' }} />
  <ColorItem
    title="theme.color.positive"
    subtitle="Green"
    colors={{
      Apple: 'rgba(102,191,60,1)',
      Apple80: 'rgba(102,191,60,.8)',
      Apple60: 'rgba(102,191,60,.6)',
      Apple30: 'rgba(102,191,60,.3)',
    }}
  />
</ColorPalette>

### View source

#### With code snippet

<Source
  language="css"
  dark
  format={false}
  code={dedent`
     .container {
       display: grid | inline-grid;
     }
  `}
/>

#### With Story Id

<Source id="atoms-logo--primaryred" />

### Render a story

We will need to use id to find the right component

<Story id="atoms-button--filled" />

### Canvas

A Canvas is a nice way to group things, adding `withSource="none"` hides the view source button

<Canvas withSource="none">
  <p>You can put anything in here really</p>
</Canvas>

<Canvas>
  <Story id="atoms-button--filled" />
</Canvas>

### Preview a component

This uses the id defined in the patterns `wingsuit.yml`

<PatternLoad patternId="button">
  {(pattern) => (
    <>
      <PatternPreview variant={pattern.getDefaultVariant()} />
    </>
  )}
</PatternLoad>

You can also customise the fields and settings passed through, and change the variant, if I wanted to show a use case for when to use the Outlined button variant and how long the button text can be:

<PatternLoad patternId="button">
  {(pattern) => (
    <>
      <PatternPreview
        variant={pattern.getVariant('outlined')}
        text="Maecenas faucibus mollis interdum Nulla vitae elit libero, a pharetra augue."
      />
      <br />
      <PatternPreview
        variant={pattern.getVariant('outlined')}
        text="Maecenas faucibus mollis &hellip;"
      />
    </>
  )}
</PatternLoad>

### View pattern properties

This will render the table showing the properties for a particular patterns variant, useful if I wanted to document each variant individually.

<PatternLoad patternId="button">
  {(pattern) => (
    <>
      <PatternProperties variant={pattern.getDefaultVariant()} />
    </>
  )}
</PatternLoad>

### View pattern include

Shows a sample twig include for the component

<PatternLoad patternId="button">
  {(pattern) => (
    <>
      <PatternInclude variant={pattern.getDefaultVariant()} />
    </>
  )}
</PatternLoad>

You can even make quick copy ones with preset fields and settings

<PatternLoad patternId="button">
  {(pattern) => (
    <>
      <PatternInclude variant={pattern.getDefaultVariant()} text="Custom props" />
    </>
  )}
</PatternLoad>

### Render the automatic documentation

Useful if you want to add more context to the components documentation but leave the default documentation in. Adding `showInclude` is optional.

<PatternLoad patternId="button">
  {(pattern) => (
    <>
      <PatternDoc pattern={pattern} showInclude />
    </>
  )}
</PatternLoad>
