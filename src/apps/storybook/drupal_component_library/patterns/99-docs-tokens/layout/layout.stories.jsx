import React from 'react';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
import docs from './docs.mdx';
import './tokens-example-layout.scss';

const layoutContent = require('./layout.twig');

export default {
  title: 'Documentation/Token reference/Layout',
  parameters: {
    viewMode: 'docs',
    previewTabs: { canvas: { hidden: true } },
    rootClasses: [],
    layout: 'padded',
    docs: {
      page: docs,
    },
  },
};

export const layout = () => null;
