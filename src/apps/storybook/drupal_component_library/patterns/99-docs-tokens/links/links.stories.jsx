import React from 'react';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
import docs from './docs.mdx';
import './tokens-example-links.scss';

const linksContent = require('./links.twig');
const linksInlineContent = require('./links--inline.twig');
const linksCustomContent = require('./links--custom.twig');

export default {
  title: 'Documentation/Token reference/Links',
  parameters: {
    rootClasses: [],
    layout: 'padded',
    docs: {
      page: docs,
    },
  },
};

export const links = () => <RenderTwig data={linksContent} />;
export const linksInline = () => <RenderTwig data={linksInlineContent} />;
export const linksCustom = () => <RenderTwig data={linksCustomContent} />;
