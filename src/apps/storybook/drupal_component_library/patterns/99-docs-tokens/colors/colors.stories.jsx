import React from 'react';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
import docs from './docs.mdx';
import './tokens-example-colors.scss';

const colorsTextContent = require('./colors--text.twig');
const colorsBackgroundContent = require('./colors--background.twig');
const colorsBorderContent = require('./colors--border.twig');

export default {
  title: 'Documentation/Token reference/Colors',
  parameters: {
    rootClasses: [],
    layout: 'padded',
    docs: {
      page: docs,
    },
  },
};

export const text_colors = () => <RenderTwig data={colorsTextContent} />;
export const background_colors = () => <RenderTwig data={colorsBackgroundContent} />;
export const border_colors = () => <RenderTwig data={colorsBorderContent} />;
