import React from 'react';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
import './tokens-example-accessibility.scss';
import docs from './docs.mdx';

const accessibilityVisuallyHiddenContent = require('./accessibility--visually-hidden.twig');
const accessibilityLinkFocusStylesContent = require('./accessibility--link-focus-styles.twig');
const accessibilityElementFocusStylesContent = require('./accessibility--element-focus-styles.twig');

export default {
  title: 'Documentation/Token reference/Accessibility',
  parameters: {
    rootClasses: [],
    layout: 'padded',
    docs: {
      page: docs,
    },
  },
};

export const visuallyHidden = () => <RenderTwig data={accessibilityVisuallyHiddenContent} />;
export const linkFocusStyles = () => <RenderTwig data={accessibilityLinkFocusStylesContent} />;
export const elementFocusStyles = () => (
  <RenderTwig data={accessibilityElementFocusStylesContent} />
);
