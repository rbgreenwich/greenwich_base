import React from 'react';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
import docs from './docs.mdx';
import './tokens-example-spacing.scss';

const customContent = require('./spacing--custom.twig');
const marginContent = require('./spacing--margin.twig');
const paddingContent = require('./spacing--padding.twig');
const flowVerticalContent = require('./spacing--flow-vertical.twig');
const flowHorizontalContent = require('./spacing--flow-horizontal.twig');
const flowCustomContent = require('./spacing--flow-custom.twig');
const utilitiesContent = require('./spacing--utilities.twig');

export default {
  title: 'Documentation/Token reference/Spacing',
  parameters: {
    rootClasses: [],
    layout: 'padded',
    docs: {
      page: docs,
    },
  },
};

export const margin = () => <RenderTwig data={marginContent} />;
export const padding = () => <RenderTwig data={paddingContent} />;
export const custom = () => <RenderTwig data={customContent} />;
export const flowVertical = () => <RenderTwig data={flowVerticalContent} />;
export const flowHorizonal = () => <RenderTwig data={flowHorizontalContent} />;
export const flowCustom = () => <RenderTwig data={flowCustomContent} />;
export const utilities = () => <RenderTwig data={utilitiesContent} />;
