import React from 'react';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
import docs from './docs.mdx';
import './tokens-example-documentation.scss';

// uncomment this to use as a way to generate compiled css for documentation
// then run make build-theme-greenwich and check the dist folder for a-component.css
// or run make build-theme-greenwich && bat -p dist/themes/drupal/greenwich_base/a-component.css

import './a-component.scss';

const documentationContent = require('./documentation.twig');

// uncomment to show in sidebar
// export default {
//   title: 'Documentation/Token reference/Documentation',
//   parameters: {
//     rootClasses: [],
//     documentation: 'padded',
//     docs: {
//       page: docs,
//     },
//   },
// };

export const documentation = () => <RenderTwig data={documentationContent} />;
