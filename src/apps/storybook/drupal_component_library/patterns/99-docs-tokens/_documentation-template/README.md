Dropping this in as a helper to generate documentation a bit faster!

Copy this file and rename `documentation`` to the name of your token.

You'll also need to uncomment lines 15-24 in the stories.jsx file to see your documentation in the sidebar
