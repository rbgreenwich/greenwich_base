import React from 'react';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
import docs from './docs.mdx';
import './tokens-example-icons.scss';

const iconsContent = require('./icons.twig');
const iconsCustomContent = require('./icons--custom.twig');

export default {
  title: 'Documentation/Token reference/Icons',
  parameters: {
    rootClasses: [],
    layout: 'padded',
    docs: {
      page: docs,
    },
  },
};

export const icons = () => <RenderTwig data={iconsContent} />;
export const iconsCustom = () => <RenderTwig data={iconsCustomContent} />;
