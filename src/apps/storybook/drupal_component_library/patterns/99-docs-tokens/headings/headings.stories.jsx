import React from 'react';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
import docs from './docs.mdx';
import './tokens-example-headings.scss';

const headingsContent = require('./headings.twig');
const headingContent = require('./headings--heading.twig');
const headingSelectorContent = require('./headings--selector.twig');

export default {
  title: 'Documentation/Token reference/Headings',
  parameters: {
    rootClasses: [],
    layout: 'padded',
    docs: {
      page: docs,
    },
  },
};

export const headings = () => <RenderTwig data={headingsContent} />;
export const heading = () => <RenderTwig data={headingContent} />;
export const headingSelector = () => <RenderTwig data={headingSelectorContent} />;
