import React from 'react';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
import docs from './docs.mdx';
import './tokens-example-body.scss';

const baseContent = require('./body--base.twig');
const textContent = require('./body--text.twig');

export default {
  title: 'Documentation/Token reference/Body',
  parameters: {
    rootClasses: [],
    layout: 'padded',
    docs: {
      page: docs,
    },
  },
};

export const body = () => <RenderTwig data={baseContent} />;
export const text = () => <RenderTwig data={textContent} />;
