import React from 'react';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
import docs from './docs.mdx';
import './tokens-example-lists.scss';

const listsContent = require('./lists.twig');

export default {
  title: 'Documentation/Token reference/Lists',
  parameters: {
    rootClasses: [],
    layout: 'padded',
    docs: {
      page: docs,
    },
  },
};

export const lists = () => <RenderTwig data={listsContent} />;
