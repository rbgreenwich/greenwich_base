import React from 'react';
import './index';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
import breakpointsFromScss from './export-breakpoints.scss';
import mdx from './docs.mdx';

const template = require('./breakpoints.twig');

const scssBreakpoints = Object.keys(breakpointsFromScss).reduce((acc, key) => {
  const [prefix, device] = key.split('--');
  if (!acc[prefix]) {
    acc[prefix] = {};
  }
  acc[prefix][device] = breakpointsFromScss[key];

  return acc;
}, {});

export default {
  title: 'Documentation/Token reference/Breakpoints',
  parameters: {
    docs: {
      page: mdx,
    },
  },
};

export const Breakpoints = () => <RenderTwig data={template} scssBreakpoints={scssBreakpoints} />;
