import React from 'react';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
import docs from './docs.mdx';
import './tokens-example-fonts.scss';

const fontsCustomContent = require('./fonts--custom.twig');
const kantumruyproContent = require('./fonts--kantumruypro.twig');

export default {
  title: 'Documentation/Token reference/Fonts and weights',
  parameters: {
    rootClasses: [],
    layout: 'padded',
    docs: {
      page: docs,
    },
  },
};

export const kantumruyPro = () => <RenderTwig data={kantumruyproContent} />;
export const fontsCustom = () => <RenderTwig data={fontsCustomContent} />;
