import React from 'react';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
import docs from './docs.mdx';
import './tokens-example-sizes.scss';

const sizesContent = require('./sizes.twig');

export default {
  title: 'Documentation/Token reference/Sizes',
  parameters: {
    rootClasses: [],
    layout: 'padded',
    docs: {
      page: docs,
    },
  },
};

export const sizes = () => <RenderTwig data={sizesContent} />;
