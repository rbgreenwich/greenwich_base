import React from 'react';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
import './tokens-example-font-size.scss';
import docs from './docs.mdx';

const fontSizeContent = require('./font-size.twig');
const fontSizeClassesContent = require('./font-size--classes.twig');

export default {
  title: 'Documentation/Token reference/Font sizes',
  parameters: {
    rootClasses: [],
    layout: 'padded',
    docs: {
      page: docs,
    },
  },
};

export const fontSize = () => <RenderTwig data={fontSizeContent} />;
export const fontSizeClasses = () => <RenderTwig data={fontSizeClassesContent} />;
