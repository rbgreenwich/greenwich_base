import React from 'react';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
import docs from './docs.mdx';
import './tokens-example-elements.scss';

const elementsContent = require('./elements.twig');

export default {
  title: 'Documentation/Token reference/Elements',
  parameters: {
    rootClasses: [],
    layout: 'padded',
    docs: {
      page: docs,
    },
  },
};

export const elements = () => <RenderTwig data={elementsContent} />;
