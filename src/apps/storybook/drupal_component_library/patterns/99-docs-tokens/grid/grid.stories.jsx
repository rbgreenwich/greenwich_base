import React from 'react';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
import docs from './docs.mdx';
import './tokens-example-grid.scss';

const gridContent = require('./grid.twig');
const gridTwoColContent = require('./grid--two-col.twig');

export default {
  title: 'Documentation/Token reference/Grid',
  parameters: {
    rootClasses: [],
    layout: 'padded',
    docs: {
      page: docs,
    },
  },
};

export const grid = () => <RenderTwig data={gridContent} />;
export const grid_Two_Col = () => <RenderTwig data={gridTwoColContent} />;
