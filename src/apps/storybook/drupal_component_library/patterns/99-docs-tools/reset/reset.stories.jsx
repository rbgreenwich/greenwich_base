import React from 'react';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
import './tokens-example-reset.scss';
import docs from './docs.mdx';

const resetContent = require('./reset.twig');

export default {
  title: 'Documentation/Tool reference/Reset',
  parameters: {
    rootClasses: [],
    layout: 'padded',
    docs: {
      page: docs,
    },
  },
};

export const reset = () => <RenderTwig data={resetContent} />;
