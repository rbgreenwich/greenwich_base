import React from 'react';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
import './tokens-example-custom-typography.scss';
import docs from './docs.mdx';

const customTypographyContent = require('./custom-typography.twig');

export default {
  title: 'Documentation/Tool reference/Custom typography',
  parameters: {
    rootClasses: [],
    layout: 'padded',
    docs: {
      page: docs,
    },
  },
};

export const customTypography = () => <RenderTwig data={customTypographyContent} />;
