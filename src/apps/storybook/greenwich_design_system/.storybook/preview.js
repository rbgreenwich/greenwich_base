import { configure, initJsBehaviors } from '@wingsuit-designsystem/storybook';
import { addDecorator, addParameters } from '@storybook/react';
import { gcdThemeingConfig, gcdThemeingDecorator } from './../theme-switcher';

initJsBehaviors('Drupal');

addParameters({
  options: {
    storySort: {
      method: 'alphabetical',
      order: [
        'Welcome',
        'Docs',
        [
          'Getting started',
          'Composing patterns',
          'Pattern definition',
          ['Overview', 'Fields', 'Settings', 'Variants', 'Configuration'],
        ],
        'Tokens',
        'Protons',
        'Atoms',
        'Molecules',
        'Organisms',
        'Templates',
        'Pages',
      ],
      locales: 'en-UK',
    },
  },

  globalTypes: {
    ...gcdThemeingConfig.globalTypes,
  },

  layout: 'padded',
});

addDecorator(gcdThemeingDecorator);

configure(
  module,
  [
    require.context('./../patterns', true, /\.stories(\.jsx|\.js|\.mdx)$/),
    require.context('./../patterns', true, /\.mdx$/),
  ],
  require.context('./config', false, /\.json|\.ya?ml$/),
  require.context('./../patterns', true, /\.twig$/)
);
