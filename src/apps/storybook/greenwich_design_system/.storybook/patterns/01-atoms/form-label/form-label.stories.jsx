import React from 'react';
import 'atoms/form-label';
import { RenderTwig } from '@wingsuit-designsystem/storybook';
const template = require('./form-label.twig');

export default {
  title: 'atoms/Form Label',
};

export const Demo = () => <RenderTwig data={template} />;
