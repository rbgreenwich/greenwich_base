const wingsuitCore = require('@wingsuit-designsystem/core');
// const postCss = require('postcss');

module.exports = {
  addons: ['@storybook/addon-docs', '@storybook/addon-essentials'],

  // we cant use refs locally because wingsuit adds stories to storybook dynanmically and doesn't generate stories.json or metadata.json files
  // but chromatic does
  refs: {
    'drupal-component-library': {
      title: 'Drupal component library',
      url: 'https://main--64a58b72475a5b1ec0f6a2d2.chromatic.com/',
      expanded: false,
    },
  },

  // // 👇 Retrieve the current environment from the configType argument
  // refs: (config, { configType }) => {
  //   if (configType === 'DEVELOPMENT') {
  //     return {
  //       'drupal-component-library': {
  //         title: 'Drupal component library',
  //         url: 'http://localhost:3009',
  //         expanded: false,
  //       },
  //     };
  //   }
  //   return {
  //     'drupal-component-library': {
  //       title: 'Drupal component library',
  //       url: 'https://main--64a58b72475a5b1ec0f6a2d2.chromatic.com/',
  //       expanded: false,
  //     },
  //   };
  // },

  webpackFinal: (config) => {
    const final = wingsuitCore.getAppPack(wingsuitCore.resolveConfig('greenwich_design_system'), [
      config,
    ]);
    return final;
  },
};
