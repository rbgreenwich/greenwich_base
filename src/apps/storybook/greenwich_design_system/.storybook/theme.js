// storybook/theme.js
import { create } from '@storybook/theming';
import logo from './logo.svg';

const { tailwind } = require('../config/silo/tailwind.json');

const { colors } = tailwind.theme;

export default create({
  base: 'light',

  // Brand assets
  brandTitle: 'Greenwich design system',
  brandUrl: 'https://www.royalgreenwich.gov.uk/',
  brandImage: logo,

  // Storybook-specific color palette
  colorPrimary: colors.primaryred.DEFAULT,
  colorSecondary: '#531E9B',

  // UI
  appBg: '#FAF9F9',
  appContentBg: '#FFFFFF',
  appBorderColor: '#EBE8E8',
  appBorderRadius: 0,

  // Fonts
  fontBase: '"Kantumruy pro", sans-serif',

  // Text colors
  textColor: '#080707',
  textInverseColor: '#FAF9F9',
  textMutedColor: '#797272',

  // Toolbar default and active colors
  barTextColor: '#080707',
  barSelectedColor: colors.primaryred.DEFAULT,
  barBg: '#EBE8E8',

  // Form colors
  buttonBg: '#ffffff',
  buttonBorder: '#EBE8E8',
  booleanBg: '#ffffff',
  booleanSelectedBg: colors.primarypurple.DEFAULT,
  inputBg: '#ffffff',
  inputBorder: '#EBE8E8',
  inputTextColor: '#080707',
  inputBorderRadius: 0,
});
