import { useEffect } from '@storybook/addons';
import React from 'react';
// import { createGlobalStyle } from 'styled-components';

/**
 * We have two ways of applying themeing in this storybook
 * One adds a class to the body mostly for tailwind benefit
 * The other switches out the css depending on which theme is selected
 */

export const gcdThemeingConfig = {
  globalTypes: {
    gcdTheme: {
      name: 'Theme',
      description: 'Global theme for components',
      defaultValue: 'website',
      toolbar: {
        icon: 'circlehollow',
        items: [
          { title: 'Website', value: 'website', right: '🕰' },
          { title: 'Greenwich community directory', value: 'gcd', right: '🧑‍🤝‍🧑' },
        ],
        title: 'Theme',
      },
    },
  },
};

/**
 * In the future we should update the webpack style
 * loader to give the style tag an id
 * @param {*} Story
 * @param {*} context
 * @returns
 */
export const gcdThemeingDecorator = (Story, context) => {
  // leaving this in as a debug method
  // eslint-disable-next-line no-unused-vars
  const listDisabledStylesheets = () => {
    const { styleSheets } = document;
    for (let i = 0; i < styleSheets.length; i += 1) {
      const currentSheet = styleSheets[i];
      const styles = currentSheet.ownerNode.innerText;
      const themeRegex = /\/\*\*\s.@theme:*(\w*)\s.*\//;
      const themeRegexMatch = styles.match(themeRegex);
      if (themeRegexMatch !== null) {
        if (currentSheet.disabled) {
          console.log(
            `❌ ${themeRegexMatch[1]} stylesheet is disabled`,
            currentSheet,
            currentSheet.disabled
          );
        }
      }
    }
  };

  /**
   * NB for this to work the theme stylesheet MUST start with /**  @theme:website \*\/
   * @param {*} theme
   */
  const switchStylesheet = (theme) => {
    const { styleSheets } = document;
    for (let i = 0; i < styleSheets.length; i += 1) {
      const currentSheet = styleSheets[i];
      const styles = currentSheet.ownerNode.innerText;
      const themeRegex = /\/\*\*\s.@theme:*(\w*)\s.*\//;
      const themeRegexMatch = styles.match(themeRegex);
      if (themeRegexMatch !== null) {
        if (themeRegexMatch[1] === theme) {
          currentSheet.disabled = false;
        } else {
          currentSheet.disabled = true;
        }
      }
    }
    listDisabledStylesheets();
  };

  useEffect(() => {
    switchStylesheet(context.globals.gcdTheme);
  }, [context.globals.gcdTheme]);

  useEffect(() => {
    switchStylesheet(context.globals.gcdTheme);
  }, []);

  // Lets you add in rootClasses for example if you were displaying an atom outside of its
  // molecule you might need some container classes to manage it

  // export const wingsuit = {
  //   patternDefinition,
  //   parameters: {
  //     rootClasses: ['xl:tw-relative', 'tw-text-white'],
  //     backgrounds: {
  //       default: 'Grey 600',
  //     },
  //   },
  // };

  const getProperty = (obj, path) => {
    let object = obj;
    const { length } = path;
    for (let i = 0; i < length; i += 1) {
      if (object == null) return false;
      object = object[path[i]];
    }
    return length ? object : false;
  };

  const rootClasses = getProperty(context, ['parameters', 'rootClasses'])
    ? getProperty(context, ['parameters', 'rootClasses'])
    : [];

  const classes = [...rootClasses, context.globals.gcdTheme].join(' ');

  const rootStyle = getProperty(context, ['parameters', 'rootStyle'])
    ? getProperty(context, ['parameters', 'rootStyle'])
    : {};

  return (
    <div data-theme={context.globals.gcdTheme} className={classes} style={rootStyle}>
      <Story />
    </div>
  );
};
