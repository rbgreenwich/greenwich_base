<?php

namespace Drupal\greenwich_base_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a global site search block.
 *
 * @Block(
 *   id = "greenwich_base_site_search",
 *   admin_label = @Translation("greenwich_base: Site Search"),
 *   category = @Translation("greenwich_base Blocks")
 * )
 */
class GreenwichBaseSearchBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => '<h2>Site Site Search Block</h2>',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
