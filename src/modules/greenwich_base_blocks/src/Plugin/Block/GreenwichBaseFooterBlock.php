<?php

namespace Drupal\greenwich_base_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a global site footer block.
 *
 * @Block(
 *   id = "greenwich_base_site_footer",
 *   admin_label = @Translation("greenwich_base: Site Footer"),
 *   category = @Translation("greenwich_base Blocks")
 * )
 */
class GreenwichBaseFooterBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => '<h2>greenwich_base Footer Block</h2>',
    ];
  }

}
