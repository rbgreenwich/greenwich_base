<?php

namespace Drupal\greenwich_base_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a global site header block.
 *
 * @Block(
 *   id = "greenwich_base_site_header",
 *   admin_label = @Translation("greenwich_base: Site Header"),
 *   category = @Translation("greenwich_base Blocks")
 * )
 */
class GreenwichBaseHeaderBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => '<h2>greenwich_base Header Block</h2>',
    ];
  }

}
