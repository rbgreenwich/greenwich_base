const path = require('path');
const glob = require('glob');
const CopyPlugin = require('copy-webpack-plugin');

function name() {
  return 'assets';
}

function webpack(appConfig) {
  const assetItems = glob.sync(
    `${appConfig.absDesignSystemPath}/**/*.{svg,png,jpg,jpeg,webp,gif,woff,woff2}`
  );

  // Storybook needs entries as array. For other apps assets keys are prefered.
  const entryPoints =
    appConfig.type === 'storybook'
      ? [assetItems]
      : {
          assets: assetItems,
        };

  return {
    entry: entryPoints,
    plugins: [
      new CopyPlugin([
        {
          from: 'images/*',
          to: appConfig.assetBundleFolder,
        },
      ]),
    ],
    module: {
      rules: [
        {
          test: /.*[/|\\\\]fonts[/|\\\\].*\.(svg|woff|woff2|eot|ttf|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                outputPath: path.join(appConfig.assetBundleFolder, 'font'),
                name: '[name].[ext]?[hash]',
              },
            },
          ],
        },
        {
          loader: 'file-loader',
          test: /.*[/|\\\\]images[/|\\\\].*\.svg$/,
          options: {
            outputPath: path.join(appConfig.assetBundleFolder, 'images'),
            name: '[name].[ext]',
          },
        },
        {
          loader: 'file-loader',
          test: /\.(png|jpg|gif|webp)$/,
          options: {
            outputPath: path.join(appConfig.assetBundleFolder, 'images'),
            name: '[name].[ext]',
          },
        },
      ],
    },
  };
}

module.exports = {
  name,
  webpack,
};
