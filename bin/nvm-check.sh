#!/bin/sh

# shellcheck shell=sh

NODE_VERSION="$(cat .nvmrc)"
ACTIVE_VERSION=$(node --version)
. ~/.nvm/nvm.sh
nvm use --silent || nvm install $(cat .nvmrc)
