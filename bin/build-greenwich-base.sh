#!/bin/sh

TARGET=build/greenwich_base
THEME_SRC=src/themes/drupal/greenwich_base
PATTERNS_SRC=dist/themes/drupal/greenwich_base

rm -rf $TARGET
mkdir -p $TARGET
cp -rp $THEME_SRC $TARGET/theme

make build-theme-greenwich
cp -rp $PATTERNS_SRC $TARGET/patterns

mv $TARGET/theme/composer.json $TARGET
