#!/bin/sh

# brew install tmux


tmux new-session -d

tmux set pane-border-status top
tmux set window-option automatic-rename on
tmux set window-option allow-rename on


tmux send-keys 'make watch-theme-greenwich' C-m
tmux pane-border-style style "bg=red"

tmux split-window -h
tmux send-keys 'make watch-storybook-greenwich' C-m


tmux -2 attach-session 
