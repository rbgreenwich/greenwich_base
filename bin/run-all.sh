#!/bin/sh

# brew install tmux

tmux new-session -d

tmux set pane-border-status top
tmux set window-option automatic-rename on
tmux set window-option allow-rename on

tmux send-keys 'make watch-theme-design-system' C-m
tmux pane-border-style style "bg=red"

tmux send-keys 'make watch-theme-greenwich' C-m

tmux split-window -h
tmux send-keys 'make watch-storybook-greenwich' C-m

tmux select-pane -t 0

tmux split-window -v -c
tmux send-keys 'make watch-theme-demo' C-m

tmux select-pane -t 2

tmux split-window -v
tmux send-keys 'make watch-storybook-demo' C-m

tmux -2 attach-session
