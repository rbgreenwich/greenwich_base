.PHONY: help list

# You can choose to use Docker for local development by specifying the
# USE_DOCKER=1 environment variable in your project .env file.
USE_DOCKER ?= 0

INCLUDE_ENV_FILE=true
ifeq (help, $(firstword $(MAKECMDGOALS)))
	INCLUDE_ENV_FILE=false
else ifeq (list, $(firstword $(MAKECMDGOALS)))
	INCLUDE_ENV_FILE=false
endif

## Optionally includes a `.env.local` file.
ifeq ($(INCLUDE_ENV_FILE), true)
-include .env.local
endif

yarn=yarn
ifeq ("${USE_DOCKER}","1")
yarn=ddev yarn
endif

# variables
ds=greenwich ## Default theme.

help: ## Lists all documented Make targets.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m\make %-30s\033[0m %s\n", $$1, $$2}'
list: help

prereq-install: ## check we have everything we need
	./bin/nvm-install.sh

prereq: ## check we have everything we need
	./bin/nvm-check.sh

start:  ## Starts the greenwich design system storybook    
	make watch-storybook-design-system

start-all: ## Start everything
	./bin/run-all.sh 

start-greenwich: ## Starts everything you need for drupal development     
	./bin/run-greenwich-base.sh 

clean: ## Remove the dist folder when things get too crazy
	[ -d dist ] && chmod -R 777 dist
	rm -rf dist

install: ## Installs the project
	make prereq
	make env
	$(yarn) install

install-no-prereq: ## Installs the project without running prereq checks
	make env
	$(yarn) install

install-ci: ## Installs the project in CI/CD environments
	make env
	$(yarn) install --cache-folder .yarn 

uninstall: ## Remove node modules and dist
	rm -rf node_modules
	rm -rf dist

env: 
	cp .env.example .env.local

tests: ## Run tests
	$(yarn) jest

generate-component: ## Generate a Wingsuit component for the design system.
	$(yarn) run prestart
	$(yarn) run ws generate-component

watch-storybook:
	$(yarn) run prestart
	$(yarn) run dev:$(ds):storybook

watch-storybook-design-system: ## Watch & build changes for the greenwich Storybook design system.
	make watch-storybook ds=design-system

watch-storybook-greenwich: ## Watch & build changes for the greenwich Storybook design system.
	make watch-storybook ds=greenwich

watch-storybook-demo: ## Watch & build changes for the demo Storybook design system.
	make watch-storybook ds=demo

watch-theme:
	$(yarn) run prestart
	$(yarn) run dev:$(ds):drupal$(if $(nostyles),:nostyles)$(if $(juststyles),:juststyles)

watch-theme-greenwich: ## Watch & build changes for the greenwich Drupal theme.
	make watch-theme ds=greenwich

watch-theme-demo: ## Watch & build changes for the demo Drupal theme.
	make watch-theme ds=demo

watch-theme-greenwich-nostyles: ## Watch & Build the greenwich_base Drupal theme without styles.
	make watch-theme ds=greenwich nostyles=true

watch-theme-greenwich-juststyles: ## Watch & build the greenwich_base styles.
	make watch-theme ds=greenwich juststyles=true

build-storybook:
	$(yarn) run prestart
	$(yarn) run build:$(ds):storybook

build-storybook-design-system: ## Build the greenwich Storybook/design system.
	make build-storybook ds=design-system

build-storybook-greenwich: ## Build the greenwich Storybook/design system.
	make build-storybook ds=greenwich

build-storybook-demo: ## Build the demo Storybook/design system.
	make build-storybook ds=demo

build-theme: 
	$(yarn) run prestart
	$(yarn) run build:$(ds):drupal$(if $(nostyles),:nostyles)$(if $(juststyles),:juststyles)

build-theme-greenwich: ## Build the greenwich_base Drupal theme.
	make build-theme ds=greenwich

build-theme-demo: ## Build the demo Drupal theme.
	make build-theme ds=demo

build-theme-greenwich-nostyles: ## Build the greenwich_base Drupal theme without styles.
	make build-theme ds=greenwich nostyles=true

build-theme-greenwich-juststyles: ## Build the greenwich_base styles. add theme=website or theme=gcd to build a specific theme
	make build-theme ds=greenwich juststyles=true

lint-js: ## Lints JS for all design systems & Drupal themes and attempts to fix any errors.
	$(yarn) run lint:js

lint-css: ## Lints CSS for all design systems & Drupal themes and attempts to fix any errors.
	$(yarn) run lint:css

lint: ## Lints the code
	$(yarn) run lint

fmt: ## Format the code
	$(yarn) run fmt
	
fmt-js: ## Format the code
	$(yarn) run fmt:js

fmt-css: ## Format the code
	$(yarn) run fmt:css

chromatic: ## Publish the built site to chromatic
	$(yarn) run chromatic	

chromatic-main: ## Publish the built main site to chromatic
	$(yarn) run chromatic-main

build-greenwich-base: ## Build the greenwich_base theme for the composer package
	./bin/build-greenwich-base.sh