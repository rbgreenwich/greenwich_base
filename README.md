# Design System Repository

For further documentation, please see the storybook site, [rbgreenwich.gitlab.io/design_system](https://rbgreenwich.gitlab.io/design_system/)

- [Design system](https://rbgreenwich.gitlab.io/design_system/)
- [greenwich_base drupal theme](https://gitlab.com/rbgreenwich/packages/greenwich_base)

## Getting started

```sh
git clone git@gitlab.com:rbgreenwich/greenwich_base.git && cd greenwich_base

# install
make install

# run
make start
```

## Commands

Run `make help` to view a list of all the commands,

| command                                 | action                                                                                                                                                  |
| --------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `make help`                             | List all commands                                                                                                                                       |
| `make install`                          | Installs the project                                                                                                                                    |
| `make start`                            | Starts the drupal component library storybook                                                                                                           |
| `make start-greenwich`                  | Starts everything you need for drupal development                                                                                                       |
| `make generate-component`               | Creates a component, see below for more information                                                                                                     |
| `make watch-storybook-greenwich`        | Loads greenwich design system storybook development task                                                                                                |
| `make watch-theme-greenwich`            | Loads greenwich_base development task to create assets for the drupal theme, optionallay provide `theme=website\|gcd` to just build one theme at a time |
| `make watch-theme-greenwich-nostyles`   | Watch greenwich theme without any styles being built                                                                                                    |
| `make watch-theme-greenwich-juststyles` | Watch greenwich theme with only styles being build, optionallay provide `theme=website\|gcd` to just build one theme at a time                          |
| `make build-storybook-greenwich`        | Builds greenwich design system storybook                                                                                                                |
| `make build-theme-greenwich`            | Builds greenwich_base for the drupal theme, optionallay provide `theme=website\|gcd` to just build one theme at a time                                  |
| `make build-theme-greenwich-nostyles`   | Build greenwich theme without any styles being built                                                                                                    |
| `make build-theme-greenwich-juststyles` | Build greenwich theme with only styles being build, optionallay provide `theme=website\|gcd` to just build one theme at a time                          |
| `make build-greenwich-base`             | Builds the final artifacts for the drupal theme package                                                                                                 |

## Troubleshooting

### Install/enable yarn

```sh
corepack enable
```

### Install NVM

To install NVM follow the instructions [here](https://github.com/nvm-sh/nvm#install--update-script). Its worth noting if you installed node from nodejs.org you may run into issues with nvm.

Don't forget about `nvm use` if you run into issues locally.

## Running tests

```sh
yarn lint
```

## Creating a new component

For more information see below

```shell
make generate-component
choose greenwich_component_library
```

# Add as a package to drupal theme

```sh
ddev composer config repositories.rbgreenwich composer https://gitlab.com/api/v4/group/rbgreenwich/-/packages/composer/packages.json

ddev composer require rbgreenwich/greenwich_base
```

# TODO

- better testing support and documentation
- [ ] create a custom generator that includes our frontend toolkit setup
