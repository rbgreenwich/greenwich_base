module.exports = {
  projects: ['<rootDir>/src/design-systems/*/jest.config.js'],

  // Everything below here is used by merging into per-design-system jest config
  verbose: true,
  testURL: 'http://localhost/',
};
