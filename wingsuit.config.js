const fs = require('fs');
const path = require('path');
const drupal = require('@wingsuit-designsystem/core/dist/server/presets/drupal');
const babel = require('@wingsuit-designsystem/core/dist/server/presets/babel');
const storybook = require('@wingsuit-designsystem/core/dist/server/presets/storybook');
const Tailwind2Json = require('@wingsuit-designsystem/preset-tailwind2/dist/plugins/Tailwind2JsonPlugin');
const twing = require('@wingsuit-designsystem/core/dist/server/presets/twing');
const { WatchIgnorePlugin } = require('webpack');
// const SpeedMeasurePlugin = require('speed-measure-webpack-plugin');
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const GreenwichNamespaces = require('./src/design-systems/greenwich/namespaces');
const GreenwichPostCssConfig = require('./src/design-systems/greenwich/postcss.config');
const DemoPostCssConfig = require('./src/design-systems/demo/postcss.config');
const DemoNamespaces = require('./src/design-systems/demo/namespaces');
const scss = require('./presets/scss');
// NB see here for 'invisible' configs! https://github.com/wingsuit-designsystem/wingsuit/blob/master/packages/core/src/stubs/defaultWingsuitConfig.stub.ts

/**
 * Ensures webpack config options are passed correctly
 * @returns string (webpack --config /Users/username/greenwich_base/src/themes/drupal/greenwich_base/webpack.config.js )
 */

function startup() {
  const startScript =
    process.env.npm_lifecycle_script !== undefined ? process.env.npm_lifecycle_script : '';
  // if we're running the ws command work out if its dev or build
  let wingsuitProcess = false;
  let processType;
  const processTypeRegex = /yarn run ws *(\w*)/;
  const processTypeMatch = startScript.match(processTypeRegex);
  if (processTypeMatch !== null) {
    wingsuitProcess = true;
    // eslint-disable-next-line prefer-destructuring
    processType = processTypeMatch[1];
  }

  let script = this.type === 'storybook' ? 'start-storybook' : 'webpack';
  let appConfigPath = path.resolve(this.absAppPath);
  let options = '';
  let configOption = '';

  // console.log(`wingsuitProcess: ${wingsuitProcess} processType: ${processType} type: ${this.type}`);
  // Handle Wingsuit CLI scripts.
  if (wingsuitProcess) {
    if (this.type === 'storybook') {
      if (processType === 'build') {
        const outputDir = `--output-dir ${this.distFolder}`;

        script = 'build-storybook';
        options = `${options} ${outputDir}`;
      } else {
        let port = ``;
        switch (this.name) {
          case 'greenwich_design_system':
            port = `--port 3007`;
            break;

          case 'drupal_component_library':
            port = `--port 3009`;
            break;

          case 'demo_component_library':
            port = `--port 3008`;
            break;

          default:
            port = ``;
            break;
        }

        options = `${options} ${port} --no-version-updates`;
      }

      configOption = `--config-dir ${appConfigPath}`;
    } else {
      if (this.configFile) {
        appConfigPath = path.resolve(appConfigPath, this.configFile);
      }

      if (this.justStyles) {
        appConfigPath = path.resolve(this.absDesignSystemPath, 'juststyles-webpack.config.js');
      }

      configOption = `--config ${appConfigPath}`;

      if (processType === 'dev') {
        options = `${options} --watch`;
      }
    }
  }

  console.log(`${script} ${configOption} ${options}`);
  return `${script} ${configOption} ${options}`;
}

const config = {
  presets: [
    scss,
    // '@wingsuit-designsystem/preset-scss',
    '@wingsuit-designsystem/preset-tailwind2',
    '@wingsuit-designsystem/preset-postcss8',
  ],
  designSystems: {
    greenwich: {
      path: 'src/design-systems/greenwich',
      patternFolder: 'patterns',
      namespaces: GreenwichNamespaces,
    },
    demo: {
      path: 'src/design-systems/demo',
      patternFolder: 'patterns',
      namespaces: DemoNamespaces,
    },
  },
  apps: {
    // ----  Storybooks
    greenwich_design_system: {
      type: 'storybook',
      path: 'src/apps/storybook/greenwich_design_system/.storybook',
      cssMode: 'hot',
      distFolder: 'dist/storybook/greenwich_design_system',
      dataFolder: './src/data',
      assetBundleFolder: '',
      designSystem: 'greenwich',
      presets: [twing, storybook],
      componentTypes: {
        wingsuit_presenter: 'Wingsuit component (UI Pattern) with presentation template',
        plain: 'Twig only component',
        plain_presenter: 'Twig only component with presentation template',
        presenter: 'Presentation template',
      },
      startup,
    },
    drupal_component_library: {
      type: 'storybook',
      path: 'src/apps/storybook/drupal_component_library/.storybook',
      cssMode: 'hot',
      distFolder: 'dist/storybook/drupal_component_library',
      dataFolder: './src/data',
      assetBundleFolder: '',
      designSystem: 'greenwich',
      presets: [twing, storybook],
      postCssConfig: {
        options: {
          postcssOptions: GreenwichPostCssConfig,
        },
      },
      componentTypes: {
        wingsuit_presenter: 'Wingsuit component (UI Pattern) with presentation template',
        plain: 'Twig only component',
        plain_presenter: 'Twig only component with presentation template',
        presenter: 'Presentation template',
      },
      noStyles: process.env.nostyles !== undefined,
      justStyles: process.env.juststyles !== undefined,
      theme: process.env.theme !== undefined ? process.env.theme : undefined,
      startup,
    },
    demo_component_library: {
      type: 'storybook',
      path: 'src/apps/storybook/demo_component_library/.storybook',
      cssMode: 'hot',
      distFolder: 'dist/storybook/demo_component_library',
      dataFolder: './src/data',
      assetBundleFolder: '',
      designSystem: 'demo',
      presets: [twing, storybook],
      postCssConfig: {
        options: {
          postcssOptions: DemoPostCssConfig,
        },
      },
      componentTypes: {
        wingsuit_presenter: 'Wingsuit component (UI Pattern) with presentation template',
        plain: 'Twig only component',
        plain_presenter: 'Twig only component with presentation template',
        presenter: 'Presentation template',
      },
      startup,
    },
    // storybook_design_system
    // storybook_brand_guidelines

    // ----  Drupal themes
    // Drupal theme - inherits patterns from greenwich [design-system]
    drupal_greenwich_base: {
      type: 'drupal',
      path: 'src/themes/drupal/greenwich_base',
      configFile: 'webpack.config.js',
      distFolder: 'dist/themes/drupal/greenwich_base',
      dataFolder: './src/data',
      cssMode: 'extract',
      assetAtomicFolder: 'atomic',
      assetBundleFolder: '',
      designSystem: 'greenwich',
      presets: [babel, drupal],
      noStyles: process.env.nostyles !== undefined,
      justStyles: process.env.juststyles !== undefined,
      theme: process.env.theme !== undefined ? process.env.theme : undefined,
      startup,
    },
    drupal_demo: {
      type: 'drupal',
      path: 'src/themes/drupal/demo',
      configFile: 'webpack.config.js',
      distFolder: 'dist/themes/drupal/demo',
      dataFolder: './src/data',
      cssMode: 'extract',
      assetAtomicFolder: 'atomic',
      assetBundleFolder: '',
      designSystem: 'demo',
      presets: [babel, drupal],
      startup,
    },

    // ----  CMS themes
    // Microsite etc
  },
  webpackFinal(appConfig, webpack) {
    let thisWebpack = webpack;
    const { designSystem } = appConfig;
    const designSystemPath = module.exports.designSystems[designSystem].path;
    const siloPath = 'config/silo';
    const configExportPath =
      appConfig.type === 'storybook'
        ? path.resolve(appConfig.path, '..', siloPath)
        : path.resolve(appConfig.path, siloPath);

    const tailwindDefault = path.resolve(''.concat(appConfig.absRootPath, '/tailwind.config'));
    const tailwindOverride = path.resolve(
      ''.concat(appConfig.absDesignSystemPath, '/tailwind.config')
    );
    const tailwindFile = fs.existsSync(''.concat(tailwindOverride, '.js'))
      ? tailwindOverride
      : tailwindDefault;

    // Replace default tailwind config lookup from wingsuit.
    thisWebpack.plugins.forEach((plugin, index) => {
      const pluginName = Object.prototype.hasOwnProperty.call(plugin, 'plugin')
        ? plugin.plugin.name
        : false;
      if (pluginName === 'Tailwind2JsonPlugin') {
        // eslint-disable-next-line new-cap
        thisWebpack.plugins[index] = new Tailwind2Json.default(
          path.resolve(tailwindFile),
          path.resolve(configExportPath, 'tailwind.json')
        );
      }
    });

    // Add tailwind config generation for the design system in use.
    thisWebpack.plugins.push(
      // eslint-disable-next-line new-cap
      new Tailwind2Json.default(
        path.resolve(tailwindFile),
        path.resolve(designSystemPath, siloPath, 'tailwind.json')
      )
    );

    // Prevent infinite loop in dev mode.
    if (appConfig.type === 'storybook') {
      thisWebpack.plugins.push(new WatchIgnorePlugin([/.*\/silo\/.*\.json/]));
    }

    // thisWebpack = {
    //   ...thisWebpack,
    //   resolve: {
    //     alias: {
    //       '@splidejs/splide': path.resolve(__dirname, './node_modules/@splidejs/splide'),
    //     },
    //   },
    // };
    // thisWebpack.resolve.alias.push({
    //   '@splidejs/splide': path.resolve(__dirname, './node_modules/@splidejs/splide'),
    // });

    // thisWebpack.module.rules.push({
    //   test: /\.html$/i,
    //   loader: 'html-loader',
    // });

    // Return altered webpack config.

    thisWebpack = {
      ...thisWebpack,
      watchOptions: {
        ignored: /node_modules/,
      },
    };

    // const smp = new SpeedMeasurePlugin();

    // console.log(JSON.stringify(thisWebpack));
    // return smp.wrap(thisWebpack);

    return thisWebpack;
  },
};
module.exports = config;
