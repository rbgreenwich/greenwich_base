const base = require('@storybook/linter-config/prettier.config');

module.exports = {
  ...base,
  arrowParens: 'always',
  endOfLine: 'auto',
  overrides: [
    {
      files: '*.html',
      options: { parser: 'babel' },
    },
  ],

  twigPrintWidth: 120,

  twigMultiTags: [
    'nav,endnav',
    'switch,case,default,endswitch',
    'ifchildren,endifchildren',
    'cache,endcache',
    'js,endjs',
  ],
};
