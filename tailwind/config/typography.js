const tailwindDefault = require('tailwindcss/defaultTheme');

module.exports = {
  theme: {
    extend: {
      fontFamily: {
        sans: ['"Kantumruy Pro"', ...tailwindDefault.fontFamily.sans],
      },
    },
  },
};
