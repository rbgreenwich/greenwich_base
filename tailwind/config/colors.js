const { brandColors } = require('./_defaults');

module.exports = {
  theme: {
    extend: {
      colors: {
        ...brandColors,
        black: '#181817',
        white: '#FEFDFD',
        transparent: 'transparent',
        current: 'currentColor',
        inherit: 'inherit',
        primary: brandColors.primaryred,
        secondary: brandColors.primarypurple,
        tertiary: brandColors.royalblue,
        error: brandColors.error['600'],
        warning: brandColors.focus['600'],
        success: brandColors.success['600'],
      },
    },
  },
};
