module.exports = {
  theme: {
    extend: {
      screens: {
        xs: '480px',
        sm: '768px',
        md: '1024px',
        lg: '1440px',
      },
    },
  },
};
