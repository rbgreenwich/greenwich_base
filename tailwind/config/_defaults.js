// https://javisperez.github.io/tailwindcolorshades/?congress-blue=004483&link-water=D9E9F8&hint-of-red=FAF9F9&malachite=20B51D&burning-orange=FF6D3F&shark=1D1D1D&bright-sun=FFD53F&daisy-bush=531E9B&alizarin-crimson=dc3338&rbgDarkBlue=042341
const tailwindColors = require('tailwindcss/colors');

const baseFontSize = 18;
const primaryred = '#dc3338';
const primarypurple = '#531E9B';
const royalblue = '#004483';
const midnightblue = '#042341';
const skyblue = '#D9E9F8';

const neutral100 = '#FFFFFF';
const neutral200 = '#FAF9F9';
const neutral300 = '#D5D2D2';
const neutral400 = '#797272';
const neutral500 = '#575252';
const neutral600 = '#383636';
const neutral700 = '#080707';

const focus = '#FFD53F';
const error = '#FF6D3F';
const success = '#20B51D';

const brandColors = {
  primaryred: {
    DEFAULT: primaryred,
    50: '#FDF3F3',
    100: '#FAE1E2',
    200: '#F4BEC0',
    300: '#EE9B9E',
    400: '#E8797C',
    500: '#E2565A',
    600: primaryred,
    700: '#B71F24',
    800: '#88171B',
    900: '#580F11',
    950: '#400B0C',
  },
  primarypurple: {
    DEFAULT: primarypurple,
    50: '#C8ABEF',
    100: '#BC9AEB',
    200: '#A677E5',
    300: '#8F55DE',
    400: '#7933D8',
    500: '#6525BD',
    600: primarypurple,
    700: '#3A156C',
    800: '#210C3D',
    900: '#07030E',
    950: '#000000',
  },
  royalblue: {
    DEFAULT: royalblue,
    50: '#64B5FF',
    100: '#50ABFF',
    200: '#2797FF',
    300: '#0084FD',
    400: '#006ED5',
    500: '#0059AC',
    600: royalblue,
    700: '#00274B',
    800: '#000A13',
    900: '#000000',
    950: '#000000',
  },
  midnightblue: {
    DEFAULT: midnightblue,
    50: '#3394F2',
    100: '#208AF1',
    200: '#0D76DB',
    300: '#0B61B4',
    400: '#094C8E',
    500: '#063867',
    600: midnightblue,
    700: '#031B33',
    800: '#021324',
    900: '#010C16',
    950: '#01080F',
  },
  skyblue: {
    DEFAULT: skyblue,
    50: '#FFFFFF',
    100: '#FFFFFF',
    200: '#FBFDFE',
    300: '#F3F8FD',
    400: '#EAF3FB',
    500: '#E2EEFA',
    600: skyblue,
    700: '#AACEEF',
    800: '#7AB2E7',
    900: '#4B97DE',
    950: '#3389D9',
  },
  neutral: {
    100: neutral100,
    200: neutral200,
    300: neutral300,
    400: neutral400,
    500: neutral500,
    600: neutral600,
    700: neutral700,
  },
  focus: {
    DEFAULT: focus,
    50: '#FFFFFF',
    100: '#FFFFFF',
    200: '#FFF9E2',
    300: '#FFF0B9',
    400: '#FFE791',
    500: '#FFDE68',
    600: focus,
    700: '#FFC907',
    800: '#CEA100',
    900: '#967500',
    950: '#7A5F00',
  },
  error: {
    DEFAULT: error,
    50: '#FFFFFF',
    100: '#FFFFFF',
    200: '#FFE9E2',
    300: '#FFCAB9',
    400: '#FFAB91',
    500: '#FF8C68',
    600: error,
    700: '#FF4207',
    800: '#CE3100',
    900: '#962400',
    950: '#7A1D00',
  },
  success: {
    DEFAULT: success,
    50: '#BFF5BE',
    100: '#AEF2AC',
    200: '#8BEC89',
    300: '#68E666',
    400: '#46E143',
    500: '#26D823',
    600: success,
    700: '#178515',
    800: '#0F540E',
    900: '#062406',
    950: '#020C02',
  },
};

const defaultColours = {
  grey: {
    ...tailwindColors.gray,
    DEFAULT: tailwindColors.gray['400'],
  },
  neutral: {
    ...tailwindColors.neutral,
    DEFAULT: tailwindColors.neutral['400'],
  },
  stone: {
    ...tailwindColors.stone,
    DEFAULT: tailwindColors.stone['400'],
  },
  red: {
    ...tailwindColors.red,
    DEFAULT: tailwindColors.red['400'],
  },
  orange: {
    ...tailwindColors.orange,
    DEFAULT: tailwindColors.orange['400'],
  },
  amber: {
    ...tailwindColors.amber,
    DEFAULT: tailwindColors.amber['400'],
  },
  yellow: {
    ...tailwindColors.yellow,
    DEFAULT: tailwindColors.yellow['400'],
  },
  lime: {
    ...tailwindColors.lime,
    DEFAULT: tailwindColors.lime['400'],
  },
  green: {
    ...tailwindColors.green,
    DEFAULT: tailwindColors.green['400'],
  },
  emerald: {
    ...tailwindColors.emerald,
    DEFAULT: tailwindColors.emerald['400'],
  },
  teal: {
    ...tailwindColors.teal,
    DEFAULT: tailwindColors.teal['400'],
  },
  cyan: {
    ...tailwindColors.cyan,
    DEFAULT: tailwindColors.cyan['400'],
  },
  sky: {
    ...tailwindColors.sky,
    DEFAULT: tailwindColors.sky['400'],
  },
  blue: {
    ...tailwindColors.blue,
    DEFAULT: tailwindColors.blue['400'],
  },
  indigo: {
    ...tailwindColors.indigo,
    DEFAULT: tailwindColors.indigo['400'],
  },
  violet: {
    ...tailwindColors.violet,
    DEFAULT: tailwindColors.violet['400'],
  },
  purple: {
    ...tailwindColors.purple,
    DEFAULT: tailwindColors.purple['400'],
  },
  fuchsia: {
    ...tailwindColors.fuchsia,
    DEFAULT: tailwindColors.fuchsia['400'],
  },
  pink: {
    ...tailwindColors.pink,
    DEFAULT: tailwindColors.pink['400'],
  },
  rose: {
    ...tailwindColors.rose,
    DEFAULT: tailwindColors.rose['400'],
  },
};
module.exports = {
  baseFontSize,
  brandColors,
  defaultColours,
};
